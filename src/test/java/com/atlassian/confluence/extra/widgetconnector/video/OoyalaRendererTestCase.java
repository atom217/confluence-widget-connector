package com.atlassian.confluence.extra.widgetconnector.video;

import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.security.PermissionManager;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

public class OoyalaRendererTestCase extends TestCase
{
    private OoyalaRenderer ooyalaRenderer;
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/simplejscript.vm";
    @Mock private VelocityRenderService velocityRenderService;
    @Mock private PermissionManager permissionManager;
    @Mock private OoyalaConfigurationManager ooyalaConfigurationManager;

    private Map<String, String> params;

    @Override
    protected void setUp() throws Exception 
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        ooyalaRenderer = new OoyalaRenderer(velocityRenderService, permissionManager, ooyalaConfigurationManager);
    }

    @Override
    protected void tearDown() throws Exception
    {
        params = null;

        super.tearDown();
    }

    public void testGetOoyalaEmbedUrlLink()
    {
        String ooyalaUrl = "http://player.ooyala.com";
        String embedCode = "55dXMxMjoBhTgtutLWhR4aG6OklYZxak";
        String ooyalaVideoLink = ooyalaUrl + "/player.js?embedCode=" + embedCode;

        ooyalaRenderer.getEmbeddedHtml(
                ooyalaVideoLink,
                params = new HashMap<String, String>()
                {
                    {
                        put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
                    }
                }
        );

        assertEquals(embedCode, params.get("embedCode"));
    }

    public void testInvalidOoyalaLinkInterpretedNull()
    {
        ooyalaRenderer.getEmbeddedHtml(
                "http://player.ooyala.com/player.js?embedCode=%22)%3B%7Dalert(document.domain)%3Bif(0)%7Bf(1%2B%22",
                params = new HashMap<String, String>()
                {
                    {
                        put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
                    }
                }
        );
        assertNull(params.get("embedCode"));
    }

    public void testGetOoyalaOoidUrlLinkTypeOne()
    {
        assertOoyalaOoidUrlLink("d0OG9mNDplZplo5ecp7_nJWwSshAmv2A");
    }

    public void testGetOoyalaOoidUrlLinkTypeTwo()
    {
        assertOoyalaOoidUrlLink("l4OG9mNDpdtpQ8phlgg0pH4DFgTTWSxt");
    }

    public void testGetOoyalaOoidUrlLinkTypeThree()
    {
        assertOoyalaOoidUrlLink("BvdWYzNjoV33e2pTitH-RtxowboRbufH");
    }

    private void assertOoyalaOoidUrlLink(String ooid)
    {
        String ooyalaUrl = "http://support.ooyala.com";
        String ooyalaVideoLink = ooyalaUrl + "/developers/otv/how-create-syndication-groups#ooid=" + ooid;

        ooyalaRenderer.getEmbeddedHtml(
                ooyalaVideoLink,
                params = new HashMap<String, String>()
                {
                    {
                        put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
                    }
                }
        );

        assertEquals(ooid, params.get("embedCode"));
    }
}
