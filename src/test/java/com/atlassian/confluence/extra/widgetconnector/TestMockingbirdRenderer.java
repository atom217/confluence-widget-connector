package com.atlassian.confluence.extra.widgetconnector;

import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.extra.widgetconnector.widgets.MockingbirdRenderer;
import junit.framework.TestCase;
import org.mockito.MockitoAnnotations;


public class TestMockingbirdRenderer extends TestCase {

    @MockitoAnnotations.Mock
    private VelocityRenderService velocityRenderService;


    public void testFriendFeed()
    {
        String webUrl = "http://gomockingbird.com/mockingbird/index.html?project=70a9785fc9d506accd6a46d039de9bc46bb3b645";
        String id = "http://gomockingbird.com/mockingbird/index.html?project=70a9785fc9d506accd6a46d039de9bc46bb3b645";

        MockingbirdRenderer renderer = new MockingbirdRenderer(velocityRenderService);
        assertEquals(id, renderer.getEmbedUrl(webUrl));
    }
}
