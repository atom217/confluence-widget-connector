package com.atlassian.confluence.extra.widgetconnector.chatter;

import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import junit.framework.TestCase;

public class FriendFeedRendererTestCase extends TestCase
{
    private String url = "http://friendfeed.com";
    private Map<String, String> resultParams = new HashMap<String, String>();
    
    private FriendFeedRenderer friendFeedRenderer;
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/simplejscript.vm";
    
    @Mock private VelocityRenderService velocityRenderService;
    
    @Override
    protected void setUp() throws Exception 
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        friendFeedRenderer = new FriendFeedRenderer(velocityRenderService);
    }
    
    public void testWithoutUsernameInUrlLink()
    {
        assertEquals("//friendfeed.com/embed/widget/?v=2&width=200", friendFeedRenderer.getEmbedUrl(url));
    }
    
    public void testWithUsernameInUrlLink()
    {
        assertEquals("//friendfeed.com/embed/widget/myname?v=2&width=200", friendFeedRenderer.getEmbedUrl(url + "/myname"));
    }
    
    public void testGetEmbeddedHtmlWithUrlThatContainsFriendFeedUsername()
    {
        String embedUrl = "//friendfeed.com/embed/widget/myname?v=2&width=200";
        
        Map<String, String> expectedParams = new HashMap<String, String>();
        expectedParams.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        
        when(velocityRenderService.render(embedUrl, expectedParams)).thenReturn("Finish executing");
        
        assertEquals("Finish executing", friendFeedRenderer.getEmbeddedHtml(url + "/myname", resultParams));
    }
}
