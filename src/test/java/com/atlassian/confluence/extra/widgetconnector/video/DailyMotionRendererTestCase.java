package com.atlassian.confluence.extra.widgetconnector.video;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import junit.framework.TestCase;

public class DailyMotionRendererTestCase extends TestCase
{
    private DailyMotionRenderer dailyMotionRenderer;
    
    @Mock private HttpRetrievalEmbedService httpRetrievalEmbedService;
    @Mock private VelocityRenderService velocityRenderService;
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        dailyMotionRenderer = new DailyMotionRenderer(httpRetrievalEmbedService, velocityRenderService);
    }
    
    public void testReturnLinkContainsEmbedParameter()
    {
        String url = "http://www.dailymotion.com/video/x7zevj_spacevidcast-2009-promo-video_tech";
        assertEquals("http://www.dailymotion.com/swf/video/x7zevj_spacevidcast-2009-promo-video_tech", dailyMotionRenderer.getEmbedUrl(url));
    }
}
