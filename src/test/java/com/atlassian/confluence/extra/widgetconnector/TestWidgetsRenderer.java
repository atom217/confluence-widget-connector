package com.atlassian.confluence.extra.widgetconnector;

import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.extra.widgetconnector.widgets.*;
import com.atlassian.renderer.v2.macro.MacroException;
import junit.framework.TestCase;
import org.mockito.MockitoAnnotations;

public class TestWidgetsRenderer extends TestCase
{
    @MockitoAnnotations.Mock
    private VelocityRenderService velocityRenderService;

    public void testGoogleGadgets() throws MacroException
    {
        String webUrl1 = "http://www.google.com/ig/directory?type=gadgets&url=hosting.gmodules.com/ig/gadgets/file/106092714974714025177/TwitterGadget.xml";
        String webUrl2 = "http://www.google.com/ig/directory?type=gadgets&url=http://hosting.gmodules.com/ig/gadgets/file/106092714974714025177/TwitterGadget.xml";
        String embedUrl = "//hosting.gmodules.com/ig/gadgets/file/106092714974714025177/TwitterGadget.xml";

        GoogleGadgetsRenderer renderer = new GoogleGadgetsRenderer(velocityRenderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl1));
        assertEquals("http:" + embedUrl, renderer.getEmbedUrl(webUrl2));
    }

    public void testBacktype() throws MacroException
    {
        String searchTerm = "atlassian";
        String user = "foo";
        String searchUrl = "http://www.backtype.com/comments?q=" + searchTerm;
        String userUrl = "http://www.backtype.com/" + user;
        String userSharedUrl = "http://www.backtype.com/" + user + "/shared";
        String embedSearchUrl = "//widgets.backtype.com/comments?q=" + searchTerm;
        String embedUserUrl = "//widgets.backtype.com/" + user;
        String embedUserSharedUrl = "//widgets.backtype.com/" + user + "/shared";

        BacktypeRenderer renderer = new BacktypeRenderer(velocityRenderService);
        assertEquals(embedSearchUrl, renderer.getEmbedUrl(searchUrl));
        assertEquals(embedUserUrl, renderer.getEmbedUrl(userUrl));
        assertEquals(embedUserSharedUrl, renderer.getEmbedUrl(userSharedUrl));
    }

    public void testWidgetbox() throws MacroException
    {
        String webUrl = "http://widgetbox.com/confluence/8e55feb2-51b3-4604-97f4-7bce293380ce";
        String idcode = "8e55feb2-51b3-4604-97f4-7bce293380ce";

        WidgetboxRenderer renderer = new WidgetboxRenderer(velocityRenderService);
        assertEquals(idcode, renderer.getIdCode(webUrl));
    }

    public void testGetSatisfaction() throws MacroException
    {
        String webUrl = "http://getsatisfaction.com/atlassian/";
        String company = "atlassian";

        GetSatisfactionRenderer renderer = new GetSatisfactionRenderer(velocityRenderService);
        assertEquals(company, renderer.getCompany(webUrl));
    }

    public void testGoogleMaps() throws MacroException
    {
        String webUrl = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Sydney+Atlassian&sll=37.764572,-122.411845&sspn=0.101507,0.22316&ie=UTF8&cd=1&ll=-33.867903,151.205285&spn=0.012882,0.027895&z=16&iwloc=A";
        String embedUrl = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Sydney+Atlassian&sll=37.764572,-122.411845&sspn=0.101507,0.22316&ie=UTF8&cd=1&ll=-33.867903,151.205285&spn=0.012882,0.027895&z=16&iwloc=A&output=embed";

        GoogleMapsRenderer renderer = new GoogleMapsRenderer(velocityRenderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

}
