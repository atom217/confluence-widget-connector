package com.atlassian.confluence.extra.widgetconnector.chatter;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.util.http.HttpRequest;
import com.atlassian.confluence.util.http.HttpResponse;
import com.atlassian.confluence.util.http.HttpRetrievalService;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.user.User;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TwitterRendererTestCase extends TestCase
{
    private static final String TWEET_ID = "0123456789"; // Fake, but has to be all digits, I think. Even the example at https://dev.twitter.com/docs/embedded-tweets suggests it.

    private static final String TWEET_MARKUP = "<blockquote>Tweet Tweet</blockquote>";

    private TwitterRenderer twitterRenderer;

    @Mock private I18NBeanFactory i18NBeanFactory;

    @Mock private LocaleManager localeManager;

    @Mock private CacheManager cacheManager;

    @Mock private WebResourceManager webResourceManager;

    @Mock private VelocityRenderService velocityRenderService;

    @Mock private HttpRetrievalService httpRetrievalService;

    @Mock Cache singleTweetsCache;

    @Mock I18NBean i18NBean;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        when(cacheManager.getCache("com.atlassian.confluence.extra.widgetconnector.chatter.TwitterRenderer")).thenReturn(singleTweetsCache);
        when(singleTweetsCache.get(TWEET_ID)).thenReturn(new TwitterRenderer.TweetRetrievalResult(true, System.currentTimeMillis(), TWEET_MARKUP, null));

        twitterRenderer = new TwitterRenderer(i18NBeanFactory, localeManager, cacheManager, webResourceManager, velocityRenderService, httpRetrievalService);
    }

    public void testRenderSingleTweetHttpsUrl()
    {
        Map<String, String> resultParams = twitterRenderer.getParameters("https://twitter.com/atlassian/status/" + TWEET_ID, new HashMap<String, String>());

        assertEquals(TWEET_MARKUP, resultParams.get("tweetHtml"));
    }

    public void testRenderSingleTweetHttpUrl()
    {
        Map<String, String> resultParams = twitterRenderer.getParameters("http://twitter.com/atlassian/status/" + TWEET_ID, new HashMap<String, String>());

        assertEquals(TWEET_MARKUP, resultParams.get("tweetHtml"));
    }

    public void testRenderSingleTweetUrlEndingWithForwardSlash()
    {
        Map<String, String> resultParams = twitterRenderer.getParameters("https://twitter.com/atlassian/status/" + TWEET_ID + "/", new HashMap<String, String>());

        assertEquals(TWEET_MARKUP, resultParams.get("tweetHtml"));
    }

    public void testRenderSingleTweetStatusesUrl()
    {
        Map<String, String> resultParams = twitterRenderer.getParameters("http://twitter.com/atlassian/statuses/" + TWEET_ID, new HashMap<String, String>());

        assertEquals(TWEET_MARKUP, resultParams.get("tweetHtml"));
    }

    public void testSingleTweetIsReadFromCacheIfAvailable()
    {
        Map<String, String> resultParams = twitterRenderer.getParameters("https://twitter.com/atlassian/status/" + TWEET_ID, new HashMap<String, String>());

        assertEquals(TWEET_MARKUP, resultParams.get("tweetHtml"));
        verifyNoMoreInteractions(httpRetrievalService);
    }

    public void testFailedTweetReadReattemptOnTimeout() throws IOException
    {
        when(singleTweetsCache.get(TWEET_ID)).thenReturn(new TwitterRenderer.TweetRetrievalResult(false, 0, null, null)).thenReturn(null);
        when(localeManager.getLocale(Mockito.<User> anyObject())).thenReturn(Locale.getDefault());

        HttpResponse twitterResponse = mock(HttpResponse.class);
        when(twitterResponse.getResponse()).thenReturn(new ByteArrayInputStream(("{ \"html\" : \"" + TWEET_MARKUP + "\" }").getBytes("UTF-8")));

        when(httpRetrievalService.get(anyString())).thenReturn(twitterResponse);

        Map<String, String> resultParams = twitterRenderer.getParameters("https://twitter.com/atlassian/status/" + TWEET_ID, new HashMap<String, String>());

        assertEquals(TWEET_MARKUP, resultParams.get("tweetHtml"));
    }

    public void testErrorMessageCached() throws IOException
    {
        when(i18NBeanFactory.getI18NBean(Mockito.<Locale> anyObject())).thenReturn(i18NBean);
        when(singleTweetsCache.get(TWEET_ID)).thenReturn(new TwitterRenderer.TweetRetrievalResult(false, System.currentTimeMillis(), null, null));

        Map<String, String> resultParams = twitterRenderer.getParameters("https://twitter.com/atlassian/status/" + TWEET_ID, new HashMap<String, String>());

        assertEquals("<div class=\"error\"><span class=\"error\">null</span> </div>", resultParams.get("tweetHtml"));

        verify(httpRetrievalService, never()).get(isA(String.class));
        verify(httpRetrievalService, never()).get(isA(HttpRequest.class));
    }
}
