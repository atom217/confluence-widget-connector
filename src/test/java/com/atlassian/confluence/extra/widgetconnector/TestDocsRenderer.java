package com.atlassian.confluence.extra.widgetconnector;

import com.atlassian.confluence.extra.widgetconnector.documents.*;
import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.renderer.v2.macro.MacroException;
import junit.framework.TestCase;
import static org.mockito.Mockito.stub;
import org.mockito.MockitoAnnotations;


public class TestDocsRenderer extends TestCase
{
    @MockitoAnnotations.Mock
    private HttpRetrievalEmbedService httpRetrievalEmbedService;

    @MockitoAnnotations.Mock
    private VelocityRenderService velocityRenderService;


    protected void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
    }

    protected void tearDown() throws Exception
    {
        httpRetrievalEmbedService = null;
    }

    public void testSlideRocket() throws MacroException
    {
        String webUrl = "http://app.sliderocket.com/app/FullPlayer.aspx?id=9A3BB11B-F958-AB30-F52D-61EE8C66EC80";
        String embedUrl = "//app.sliderocket.com/app/FullPlayer.aspx?id=9A3BB11B-F958-AB30-F52D-61EE8C66EC80";

        SlideRocketRenderer renderer = new SlideRocketRenderer(velocityRenderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testSlideShare() throws MacroException
    {
        String httpEmbedId = "123";
        String webUrl = "http://www.slideshare.net/dgriffith/promotions-20-the-future-of-interactive-marketing-1008197";
        String embedUrl = "//static.slideshare.net/swf/ssplayer2.swf?doc=" + httpEmbedId;

        SlideShareRenderer renderer = new SlideShareRenderer(httpRetrievalEmbedService, velocityRenderService);
        stub(httpRetrievalEmbedService.getEmbedData(webUrl, SlideShareRenderer.PATTERN, SlideShareRenderer.class.getName())).toReturn(httpEmbedId);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testScribd() throws MacroException
    {
        String httpEmbedId = "123";
        String docId = "12345";
        String webUrl = "http://www.scribd.com/doc/" + docId + "/Paperclay-Worm-Snail";
        String embedUrl = "//documents.scribd.com/ScribdViewer.swf?document_id=" + docId + "&access_key=" + httpEmbedId + "&version=1";

        ScribdRenderer renderer = new ScribdRenderer(httpRetrievalEmbedService, velocityRenderService);
        stub(httpRetrievalEmbedService.getEmbedData(webUrl, ScribdRenderer.ACCESS_KEY_PATTERN, ScribdRenderer.class.getName())).toReturn(httpEmbedId);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testGoogleDocsPresentation() throws MacroException
    {
        String docId = "1wFSW4MAv8bDNUaylGKtsKw5RJ-UD3nmDtFlI9wngW28";
        String webUrl = "http://docs.google.com/presentation/d/" + docId;
        String embedUrl = "//docs.google.com/presentation/d/" + docId + "/embed";

        GoogleDocsRenderer renderer = new GoogleDocsRenderer(velocityRenderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testGoogleDocsDocument() throws MacroException
    {

        String docId = "1GxWTMUYBiBRzVlzdfdX9mDAgF0w_iwkCU6ZNKtitl3k";
        String webUrl = "http://docs.google.com/document/d/" + docId;
        String embedUrl = "//docs.google.com/document/d/" + docId + "/pub?embedded=true";

        GoogleDocsRenderer renderer = new GoogleDocsRenderer(velocityRenderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testDabbleDb() throws MacroException
    {
        final String idcode = "KHzdBpjU";
        String webUrl = "https://widgetconnectorx.dabbledb.com/page/widgetconnectorx/" + idcode;

        String embedUrl= "https://widgetconnectorx.dabbledb.com/page/widgetconnectorx/" + idcode + "?embed=true";

        DabbleDbRenderer renderer = new DabbleDbRenderer(velocityRenderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testShareAcrobat() throws MacroException
    {
        final String idcode = "24eaa078-2d86-4de2-9d37-2a5572203a46";
        String webUrl = "https://share.acrobat.com/adc/document.do?docid=" + idcode;

        String flashVars= "docId=24eaa078-2d86-4de2-9d37-2a5572203a46";

        ShareAcrobat renderer = new ShareAcrobat(velocityRenderService);
        assertEquals(flashVars, renderer.getFlashVars(webUrl));
    }

    public void testVoiceThread() throws MacroException
    {
        String webUrl = "http://voicethread.com/share/1191/";
        String embedUrl = "//voicethread.com/book.swf?b=1191";

        VoiceThreadRenderer renderer = new VoiceThreadRenderer(velocityRenderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testGoogleSpreadSheet() throws MacroException
    {
        String key = "rW9tWYp5SBl-XoDDjsGE9fw";
        String embedUrl = "//docs.google.com/spreadsheet/pub?key=" + key + "&output=html&widget=true&element=true&gid=0";
        GoogleSpreadsheetsRenderer renderer = new GoogleSpreadsheetsRenderer(velocityRenderService);

        String oldWebUrl = "http://spreadsheets.google.com/pub?key=rW9tWYp5SBl-XoDDjsGE9fw&output=html";
        assertTrue(oldWebUrl + " doesn't match Google Spreadsheet", renderer.matches(oldWebUrl));
        assertEquals(embedUrl, renderer.getEmbedUrl(oldWebUrl));

        String newWebUrl = "http://docs.google.com/spreadsheet/pub?key=rW9tWYp5SBl-XoDDjsGE9fw&output=html";
        assertTrue(newWebUrl + " doesn't match Google Spreadsheet", renderer.matches(newWebUrl));
        assertEquals(embedUrl, renderer.getEmbedUrl(newWebUrl));

        String domainWebUrl = "https://docs.google.com/a/atlassian.com/spreadsheet/ccc?key=rW9tWYp5SBl-XoDDjsGE9fw&output=html";
        assertTrue(domainWebUrl + " doesn't match Google Spreadsheet", renderer.matches(domainWebUrl));
        assertEquals(embedUrl, renderer.getEmbedUrl(domainWebUrl));
    }
}
