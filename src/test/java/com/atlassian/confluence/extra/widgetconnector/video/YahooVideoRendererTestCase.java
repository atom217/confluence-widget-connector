package com.atlassian.confluence.extra.widgetconnector.video;

import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import junit.framework.TestCase;

public class YahooVideoRendererTestCase extends TestCase
{
    private YahooVideoRenderer yahooVideoRenderer;
    private static final String EMBED_URL = "//d.yimg.com/static.video.yahoo.com/yep/YV_YEP.swf?ver=2.2.34";
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/embed.vm";
    
    @Mock private VelocityRenderService velocityRenderService;

    @Override
    protected void setUp() throws Exception 
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        yahooVideoRenderer = new YahooVideoRenderer(velocityRenderService);
    }
    
    public void testGetRenderedHtmlWithYahooVideoLink()
    {
        String url = "video.yahoo.com/watch/1234567/7654321";

        Map<String, String> params = new HashMap<String, String>();

        Map<String, String> expectedParams = new HashMap<String, String>();
        expectedParams.put("flashVars", "id=7654321&vid=1234567&lang=en-us&intl=us&embed=1");
        expectedParams.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);

        when(velocityRenderService.render(EMBED_URL, expectedParams)).thenReturn("Finish Rendering");

        assertEquals("Finish Rendering", yahooVideoRenderer.getEmbeddedHtml(url, params));
    }
}
