package com.atlassian.confluence.extra.widgetconnector.documents;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import junit.framework.TestCase;

public class ScribdRendererTestCase extends TestCase
{
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/scribd.vm";
    
    private ScribdRenderer scribdRenderer;
    
    private String httpEmbedId = "123";
    private String docId = "12345";
    private String url = "http://www.scribd.com/doc/" + docId + "/Paperclay-Worm-Snail";
    private String embedUrl = "//documents.scribd.com/ScribdViewer.swf?document_id=" + docId + "&access_key=" + httpEmbedId + "&version=1";
    private String message = "Done Executing";
    
    private Map<String, String> params, expectedParam;
    
    @Mock private VelocityRenderService velocityRenderService;
    @Mock private HttpRetrievalEmbedService httpRetrievalEmbedService;
    
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        params = new HashMap<String, String>();
        expectedParam = new HashMap<String, String>();
        
        expectedParam.put("_template", VELOCITY_TEMPLATE);
        expectedParam.put("nameHtml", docId);
        
        scribdRenderer = new ScribdRenderer(httpRetrievalEmbedService, velocityRenderService);
        
        when(httpRetrievalEmbedService.getEmbedData(url, scribdRenderer.ACCESS_KEY_PATTERN, scribdRenderer.getClass().getName())).thenReturn(httpEmbedId);
        when(velocityRenderService.render(embedUrl, expectedParam)).thenReturn(message);
    }
    
    public void testRenderWithCustomDimensions()
    {
        params.put("width", "400");
        params.put("height", "300");
        
        expectedParam.put("width", "400");
        expectedParam.put("height", "300");
        
        assertEquals(message, scribdRenderer.getEmbeddedHtml(url, params));
    }
    
    public void testRenderWithDefaultDimensions()
    {
        expectedParam.put("width", "475");
        expectedParam.put("height", "355");
        
        assertEquals(message, scribdRenderer.getEmbeddedHtml(url, params));
    }
}
