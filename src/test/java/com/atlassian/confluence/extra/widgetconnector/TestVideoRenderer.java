package com.atlassian.confluence.extra.widgetconnector;

import static org.mockito.Mockito.stub;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.confluence.extra.widgetconnector.services.DefaultPlaceholderService;
import com.atlassian.confluence.extra.widgetconnector.services.PlaceholderService;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import junit.framework.TestCase;

import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.extra.widgetconnector.video.BlipRenderer;
import com.atlassian.confluence.extra.widgetconnector.video.EpisodicRenderer;
import com.atlassian.confluence.extra.widgetconnector.video.GoogleVideoRenderer;
import com.atlassian.confluence.extra.widgetconnector.video.MetacafeRenderer;
import com.atlassian.confluence.extra.widgetconnector.video.MySpaceVideoRenderer;
import com.atlassian.confluence.extra.widgetconnector.video.TudouRenderer;
import com.atlassian.confluence.extra.widgetconnector.video.ViddlerRenderer;
import com.atlassian.confluence.extra.widgetconnector.video.VimeoRenderer;
import com.atlassian.confluence.extra.widgetconnector.video.YahooVideoRenderer;
import com.atlassian.confluence.extra.widgetconnector.video.YoutubeRenderer;
import com.atlassian.confluence.util.http.HttpRequest;
import com.atlassian.confluence.util.http.HttpResponse;
import com.atlassian.confluence.util.http.HttpRetrievalService;
import com.atlassian.confluence.util.http.httpclient.HttpClientHttpResponse;
import com.atlassian.renderer.v2.macro.MacroException;


public class TestVideoRenderer extends TestCase
{
    @MockitoAnnotations.Mock
    private HttpRetrievalEmbedService httpRetrievalEmbedService;

    @MockitoAnnotations.Mock
    private VelocityRenderService velocityRenderService;

    @MockitoAnnotations.Mock
    private HttpRetrievalService httpRetrievalService;

    @MockitoAnnotations.Mock
    private PlaceholderService placeholderService;

    @MockitoAnnotations.Mock
    private I18NBeanFactory i18NBeanFactory;

    @MockitoAnnotations.Mock
    private LocaleManager localeManager;

    protected void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
    }

    protected void tearDown() throws Exception
    {
        httpRetrievalEmbedService = null;
    }

    public void testYoutube() throws MacroException
    {
        String webUrl = "http://au.youtube.com/watch?v=dHi-ZcvFV_0&feature=rec-fresh";
        String embedUrl = "//www.youtube.com/embed/dHi-ZcvFV_0?wmode=opaque";

        YoutubeRenderer renderer = new YoutubeRenderer(velocityRenderService, placeholderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }


    public void testYoutubePlaceholder() throws MacroException
    {
        String webUrl     = "http://au.youtube.com/watch?v=dHi-ZcvFV_0&feature=rec-fresh";
        String thumbUrl   = "http://img.youtube.com/vi/dHi-ZcvFV_0/mqdefault.jpg";
        String overlayUrl = "youtube";
        int    width      = 400;
        int    height     = 300;

        String commandUrl = "/plugins/servlet/widgetconnector/placeholder?" +
                "thumb=" + thumbUrl +
                "&overlay=" + overlayUrl +
                "&width=" + width +
                "&height=" + height;

        placeholderService = new DefaultPlaceholderService(httpRetrievalService);

        YoutubeRenderer renderer   = new YoutubeRenderer(velocityRenderService, placeholderService);
        Map<String, String> params = new HashMap<String, String>();
        ImagePlaceholder iph       = renderer.getImagePlaceholder(webUrl, params);

        assertEquals(commandUrl, iph.getUrl());
    }

    public void testVimeo() throws MacroException
    {
        String webUrl = "http://www.vimeo.com/1970617";
        String embedUrl = "//vimeo.com/moogaloop.swf?clip_id=1970617&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1";

        VimeoRenderer renderer = new VimeoRenderer(velocityRenderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testMetacafe() throws MacroException
    {
        String webUrl = "http://www.metacafe.com/watch/1926002/funny_commercial_dirt_free/";
        String embedUrl = "//www.metacafe.com/fplayer/1926002/funny_commercial_dirt_free.swf";

        MetacafeRenderer renderer = new MetacafeRenderer(velocityRenderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testGoogleVideo() throws MacroException
    {
        String webUrl = "http://video.google.com/videoplay?docid=7134231252031614004&hl=en";
        String embedUrl = "//video.google.com/googleplayer.swf?docid=7134231252031614004&hl=en&fs=true";

        GoogleVideoRenderer renderer = new GoogleVideoRenderer(velocityRenderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testEpisodicOldVideo() throws MacroException, IOException
    {
        String webUrl = "http://app.episodic.com/shows/13/episodes/41";
        String webUrl2 = "http://app.episodic.com/shows/lme2ula7pdkx/episodes/ktiv4j9p58n7";
        String embedUrl = "//cdn.episodic.com/player/EpisodicPlayer.swf?config=http%3A%2F%2Fcdn.episodic.com%2Fshows%2F13%2F41%2fconfig.xml";
        String embedUrl2 = "//cdn.episodic.com/player/EpisodicPlayer.swf?config=http%3A%2F%2Fcdn.episodic.com%2Fshows%2Flme2ula7pdkx%2Fktiv4j9p58n7%2fconfig.xml";
        String configXmlUrl = "http://cdn.episodic.com/shows/13/41/config.xml";
        String configXmlUrl2 = "http://cdn.episodic.com/shows/lme2ula7pdkx/ktiv4j9p58n7/config.xml";
        HttpMethod getMethod = new GetMethod()
    	{
	    	@Override
	    	public int getStatusCode()
	    	{
	            return 200;
	    	}
        };

        EpisodicRenderer renderer = new EpisodicRenderer(velocityRenderService, httpRetrievalService);
        HttpRequest req = httpRetrievalService.getDefaultRequestFor(configXmlUrl);
        HttpRequest req2 = httpRetrievalService.getDefaultRequestFor(configXmlUrl2);
        HttpResponse resp = new HttpClientHttpResponse(req,  getMethod);
        HttpResponse resp2 = new HttpClientHttpResponse(req2,  getMethod);
        when(httpRetrievalService.get(req)).thenReturn(resp);
        when(httpRetrievalService.get(req2)).thenReturn(resp2);

        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
        assertEquals(embedUrl2, renderer.getEmbedUrl(webUrl2));
    }

    /**
     * test new URL format in Episodic, see WC-16
     */
    public void testEpisodicNewVideo() throws MacroException, IOException
    {
    	String episodicUrlWithNewFormat = "http://app.episodic.com/shows/13/episodes/41";
        String episodicUrlWithNewFormat2 = "http://app.episodic.com/shows/lme2ula7pdkx/episodes/ktiv4j9p58n7";
        String embedUrlWithNewFormat = "//cdn.episodic.com/player/EpisodicPlayer.swf?config=http%3A%2F%2Fcdn.episodic.com%2Fshows%2F13%2F1%2F41%2fconfig.xml";
        String embedUrlWithNewFormat2 = "//cdn.episodic.com/player/EpisodicPlayer.swf?config=http%3A%2F%2Fcdn.episodic.com%2Fshows%2Flme2ula7pdkx%2F7%2Fktiv4j9p58n7%2fconfig.xml";
        String configXmlUrl = "http://cdn.episodic.com/shows/13/41/config.xml";
        String configXmlUrl2 = "http://cdn.episodic.com/shows/lme2ula7pdkx/ktiv4j9p58n7/config.xml";
        HttpMethod getMethod = new GetMethod()
    	{
	    	@Override
	    	public int getStatusCode()
	    	{
	            return 404;
	    	}
        };

        EpisodicRenderer renderer = new EpisodicRenderer(velocityRenderService, httpRetrievalService);
        HttpRequest req = httpRetrievalService.getDefaultRequestFor(configXmlUrl);
        HttpRequest req2 = httpRetrievalService.getDefaultRequestFor(configXmlUrl2);
        HttpResponse resp = new HttpClientHttpResponse(req,  getMethod);
        HttpResponse resp2 = new HttpClientHttpResponse(req2,  getMethod);
        when(httpRetrievalService.get(req)).thenReturn(resp);
        when(httpRetrievalService.get(req2)).thenReturn(resp2);

        assertEquals(embedUrlWithNewFormat, renderer.getEmbedUrl(episodicUrlWithNewFormat));
        assertEquals(embedUrlWithNewFormat2, renderer.getEmbedUrl(episodicUrlWithNewFormat2));
    }
    
    public void testMySpace() throws MacroException
    {
        String webUrl = "http://myspace.com/theflynn/video/glacier-creek-confluence-time-lapse-/3376248";
        String embedUrl = "//myspace.com/play/video/glacier-creek-confluence-time-lapse-3376248";

        MySpaceVideoRenderer renderer = new MySpaceVideoRenderer(velocityRenderService, i18NBeanFactory, localeManager);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testViddler() throws MacroException
    {
        String httpEmbedId = "123";
        String webUrl = "http://www.viddler.com/explore/jeremyfranklin/videos/4/";
        String embedUrl = "//www.viddler.com/player/" + httpEmbedId;

        ViddlerRenderer renderer = new ViddlerRenderer(httpRetrievalEmbedService, velocityRenderService);
        stub(httpRetrievalEmbedService.getEmbedData(webUrl, ViddlerRenderer.PATTERN, ViddlerRenderer.class.getName())).toReturn(httpEmbedId);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testBlip() throws MacroException
    {
        String httpEmbedId = "123";
        String webUrl = "http://blip.tv/file/1435917";
        String ajaxUrl = "http://blip.tv/file/1435917"+"?skin=json";
        String embedUrl = "//blip.tv/play/" + httpEmbedId;

        BlipRenderer renderer = new BlipRenderer(httpRetrievalEmbedService, velocityRenderService);
        stub(httpRetrievalEmbedService.getEmbedData(ajaxUrl, BlipRenderer.PATTERN, BlipRenderer.class.getName())).toReturn(httpEmbedId);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

    public void testYahooVideo() throws MacroException
    {
        String webUrl = "http://video.yahoo.com/watch/4422504/11856052";
        String flashVars = "id=11856052&vid=4422504&lang=en-us&intl=us&embed=1";

        YahooVideoRenderer renderer = new YahooVideoRenderer(velocityRenderService);
        assertEquals(flashVars, renderer.getFlashVars(webUrl));
    }

    public void testTudou() throws MacroException
    {
        String webUrl = "http://www.tudou.com/v/5DDj1sfSxzs";
        String embedUrl = "//tudou.com/v/5DDj1sfSxzs&hl=en&fs=1";

        TudouRenderer renderer = new TudouRenderer(velocityRenderService);
        assertEquals(embedUrl, renderer.getEmbedUrl(webUrl));
    }

}