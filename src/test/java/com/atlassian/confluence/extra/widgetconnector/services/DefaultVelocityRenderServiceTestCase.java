package com.atlassian.confluence.extra.widgetconnector.services;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

public class DefaultVelocityRenderServiceTestCase extends TestCase
{
    private DefaultVelocityRenderService defaultVelocityRenderService;
    private final String url = "www.atlassian.com";
    
    private Map<String, Object> contextMap = new HashMap<String, Object>();
    private Map<String, String> params = new HashMap<String, String>();
    
    public void testRenderWithDefaultDimensions()
    {
        defaultVelocityRenderService = new TestDefaultVelocityRenderService()
        {
            @Override
            protected String getRenderedTemplate(String template,
                    Map<String, Object> contextMap)
            {
                assertEquals("com/atlassian/confluence/extra/widgetconnector/templates/embed.vm", template);
                
                assertEquals(url, contextMap.get("urlHtml"));
                assertEquals("400", contextMap.get("width"));
                assertEquals("300", contextMap.get("height"));
                
                return "Finish executing";
            }
        };
        
        assertEquals("Finish executing", defaultVelocityRenderService.render(url, params));
    }
    
    public void testRenderWithCustomDimensions()
    {
        params.put("width", "800");
        params.put("height", "600");
        params.put("_template", "com/atlassian/confluence/custom.vm");
        
        defaultVelocityRenderService = new TestDefaultVelocityRenderService()
        {
            @Override
            protected String getRenderedTemplate(String template,
                    Map<String, Object> contextMap)
            {
                assertEquals("com/atlassian/confluence/custom.vm", template);
                
                assertEquals(url, contextMap.get("urlHtml"));
                assertEquals("800", contextMap.get("width"));
                assertEquals("600", contextMap.get("height"));
                
                return "Finish executing";
            }
        };
        
        assertEquals("Finish executing", defaultVelocityRenderService.render(url, params));
    }
    
    private class TestDefaultVelocityRenderService extends DefaultVelocityRenderService
    {
        @Override
        protected Map<String, Object> getDefaultVelocityContext()
        {
            return contextMap;
        }
    }
}
