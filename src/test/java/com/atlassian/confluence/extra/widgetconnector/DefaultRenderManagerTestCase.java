package com.atlassian.confluence.extra.widgetconnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import junit.framework.TestCase;

public class DefaultRenderManagerTestCase extends TestCase
{
    private DefaultRenderManager defaultRenderManager;
    private List<WidgetRenderer> renderSupporter;
    private Map<String, Object> contextMap;
    
    @Mock private WidgetRenderer widgetRenderer;
    
    @Override
    protected void setUp() throws Exception 
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        renderSupporter = new ArrayList<WidgetRenderer>();
        
        contextMap = new HashMap<String, Object>();
        
        renderSupporter.add(widgetRenderer);
    }
    
    public void testEmbeddedHtmlThatMatchesBaseUrlPattern()
    {
        final String url = "https://www.atlassian.com/confluence";
        
        Map<String, String> params = new HashMap<String, String>();
        
        defaultRenderManager = new DefaultRenderManager(renderSupporter)
        {
            @Override
            protected String getRenderedTemplate(Map<String, Object> contextMap) 
            {
                assertEquals(url, contextMap.get("urlHtml"));
                assertEquals("www.atlassian.com", contextMap.get("baseUrlHtml"));
                return "Finish Rendering Template";
            }

            @Override
            protected Map<String, Object> getDefaultVelocityContext() {
                return contextMap;
            }
        };
        
        assertEquals("Finish Rendering Template", defaultRenderManager.getEmbeddedHtml(url, params));
    }
}
