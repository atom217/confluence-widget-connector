package com.atlassian.confluence.extra.widgetconnector;

import com.atlassian.confluence.extra.widgetconnector.photo.FlickrRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import junit.framework.TestCase;
import static org.mockito.Mockito.stub;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class TestFlickrRenderer extends TestCase
{
    private static String TAG = "art";
    private static String USERNAME = "batman";
    private static String USERID = "18422804@N00";
    private static String PHOTOID = "12345";


    private FlickrRenderer flickrRenderer;
    @MockitoAnnotations.Mock
    private HttpRetrievalEmbedService httpRetrievalEmbedService;

    @MockitoAnnotations.Mock
    private VelocityRenderService velocityRenderService;

    protected void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        flickrRenderer = new FlickrRenderer(httpRetrievalEmbedService, velocityRenderService);
    }

    public void testFlickrSet() throws IOException
    {
        String webUrl = "http://flickr.com/photos/" + USERNAME + "/sets/123";
        String embedUrl = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2F" + USERNAME + "%2Fsets%2F123%2Fshow%2F&page_show_back_url=%2Fphotos%2F" + USERNAME + "%2Fsets%2F123%2F&set_id=123&jump_to=";
        assertEquals(embedUrl, flickrRenderer.getEmbedUrl(webUrl));
    }

    public void testFlickrUser() throws IOException
    {
        String webUrl = "http://flickr.com/photos/" + USERNAME;
        String embedUrl = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2F" + USERNAME + "%2Fshow%2F&page_show_back_url=%2Fphotos%2F" + USERNAME + "%2F&user_id=" + USERID + "&jump_to=";

        stub(httpRetrievalEmbedService.getEmbedData("https://www.flickr.com/photos/" + USERNAME, FlickrRenderer.PATTERN, FlickrRenderer.class.getName())).toReturn(USERID);
        assertEquals(embedUrl, flickrRenderer.getEmbedUrl(webUrl));
    }

    public void testFlickrUserWithPhoto() throws IOException
    {
        String webUrl = "http://flickr.com/photos/" + USERNAME + "/12345";
        String embedUrl = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2F" + USERNAME + "%2Fshow%2Fwith%2F" + PHOTOID + "%2F&page_show_back_url=%2Fphotos%2F" + USERNAME + "%2Fwith%2F" + PHOTOID + "%2F&user_id=" + USERID + "&jump_to=" + PHOTOID;
        stub(httpRetrievalEmbedService.getEmbedData("https://www.flickr.com/photos/" + USERNAME, FlickrRenderer.PATTERN, FlickrRenderer.class.getName())).toReturn(USERID);
        assertEquals(embedUrl, flickrRenderer.getEmbedUrl(webUrl));
    }

    public void testFlickrTag() throws IOException
    {
        String webUrl = "http://flickr.com/photos/tags/" + TAG;
        String embedUrl = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2Ftags%2F" + TAG + "%2Fshow%2F&page_show_back_url=%2Fphotos%2Ftags%2F" + TAG + "%2F&tags=" + TAG + "&jump_to=&st" + TAG + "_index=";
        assertEquals(embedUrl, flickrRenderer.getEmbedUrl(webUrl));
    }


    public InputStream getIdPageExcerpt()
    {
        String flickrPage = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<title>Flickr: Your Photostream</title>\n" +
                "\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                "\t<meta name=\"keywords\" content=\"photography...\">\t\n" +
                "\t<meta name=\"description\" content=\"Flickr is almost ...\">\n" +
                "\t<meta http-equiv=\"imagetoolbar\" content=\"no\">\n" +
                "\t<meta name=\"viewport\" content=\"width=820\" />\n" +
                "\t<link href=\"http://l.yimg.com/g/css/c_flickr.css.v62448.14\" rel=\"stylesheet\" type=\"text/css\">\n" +
                "\t<link href=\"http://l.yimg.com/g/css/c_formatting_tips.css.v61113.14\" rel=\"stylesheet\" type=\"text/css\">\n" +
                "\t<link rel=\"alternate\"\ttype=\"application/atom+xml\" title=\"Flickr: Your Photostream Atom feed\" href=\"http://api.flickr.com/services/feeds/photos_public.gne?id=" + USERID + "&amp;lang=en-us&amp;format=atom\">\n" +
                "\t<link rel=\"alternate\"\ttype=\"application/rss+xml\" title=\"Flickr: Your Photostream RSS feed\" href=\"http://api.flickr.com/services/feeds/photos_public.gne?id=" + USERID + "&amp;lang=en-us&amp;format=rss_200\">";
        byte currentXMLBytes[] = flickrPage.getBytes();
        return new ByteArrayInputStream(currentXMLBytes);
    }
}
