package com.atlassian.confluence.extra.widgetconnector.video;

import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import junit.framework.TestCase;

public class TudouRendererTestCase extends TestCase
{
    private TudouRenderer tudouRenderer;
    private Map<String, String> params;
    
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/embed.vm";
    
    @Mock private VelocityRenderService velocityRenderService;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        params = new HashMap<String, String>();
        
        tudouRenderer = new TudouRenderer(velocityRenderService);
    }
    
    public void testDefaultVelocityTemplatePath()
    {
        String url = "http://tudou.com/v/5DDj1sfSxzs";
        String embedUrl = "//tudou.com/v/5DDj1sfSxzs&hl=en&fs=1";

        Map<String, String> expectedParam = new HashMap<String, String>();
        expectedParam.put("_template", VELOCITY_TEMPLATE);

        when(velocityRenderService.render(embedUrl, expectedParam)).thenReturn("Done Executing");

        assertEquals("Done Executing", tudouRenderer.getEmbeddedHtml(url, params));
    }
}
