package com.atlassian.confluence.extra.widgetconnector;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import junit.framework.TestCase;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class WidgetMacroTestCase extends TestCase
{
    private WidgetMacro widgetMacro;
    @Mock private DefaultRenderManager renderManager;
    @Mock private RenderContext renderContext;
    @Mock private LocaleManager localeManager;
    @Mock private I18NBeanFactory i18NBeanFactory;
    @Mock private I18NBean i18NBean;
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        widgetMacro = new WidgetMacro(renderManager, localeManager, i18NBeanFactory);

        when(i18NBeanFactory.getI18NBean(Matchers.<Locale> anyObject())).thenReturn(i18NBean);
        when(i18NBean.getText(anyString())).thenAnswer(
                new Answer<Object>()
                {
                    @Override
                    public Object answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0];
                    }
                }
        );
    }
    
    public void testErrorMessageWhenUrlIsEmpty() throws MacroException
    {
        String errorMessage = "<div class=\"error\"><span class=\"error\">macro.error.urlnotspecified</span> </div>";
        
        Map<String, String> params = new HashMap<String, String>();
        
        assertEquals(errorMessage, widgetMacro.execute(params, "", renderContext));
    }
    
    public void testRenderedHtmlWhereUrlIsFromBodyContent() throws MacroException
    {
        String body = "www.atlassian.com";
        
        Map<String, String> params = new HashMap<String, String>();
        
        when(renderManager.getEmbeddedHtml(body, params)).thenReturn("Finish rendering");
        
        assertEquals("Finish rendering", widgetMacro.execute(params, body, renderContext));
    }
}
