package com.atlassian.confluence.extra.widgetconnector;

import com.atlassian.confluence.extra.widgetconnector.chatter.FriendFeedRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import junit.framework.TestCase;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;


public class TestChatterRenderer extends TestCase
{
    private Map params = new HashMap();
    
    @MockitoAnnotations.Mock
    private VelocityRenderService velocityRenderService;


    public void testFriendFeed()
    {
        String webUrl = "http://friendfeed.com/batman";
        String id = "//friendfeed.com/embed/widget/batman?v=2&width=200";

        FriendFeedRenderer renderer = new FriendFeedRenderer(velocityRenderService);
        assertEquals(id, renderer.getEmbedUrl(webUrl));
    }
}
