package com.atlassian.confluence.extra.widgetconnector.documents;

import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import junit.framework.TestCase;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

public class GoogleCalendarRendererTestCase extends TestCase
{
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/iframe.vm";
    
    private GoogleCalenderRenderer googleCalenderRenderer;

    private String calId = "foo%40gmail.com";
    private String url = "http://www.google.com/calendar/embed?src=" + calId + "&ctz=Australia/Sydney";
    private String message = "Done Executing";
    
    private Map<String, String> params, expectedParam;
    
    @Mock private VelocityRenderService velocityRenderService;
    
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        params = new HashMap<String, String>();
        expectedParam = new HashMap<String, String>();
        expectedParam.put("_template", VELOCITY_TEMPLATE);
        
        googleCalenderRenderer = new GoogleCalenderRenderer(velocityRenderService);
        
        when(velocityRenderService.render(url, expectedParam)).thenReturn(message);
    }
    
    public void testRenderWithCustomDimensions()
    {
        params.put("width", "400px");
        params.put("height", "300px");
        
        expectedParam.put("width", "400px");
        expectedParam.put("height", "300px");
        
        assertEquals(message, googleCalenderRenderer.getEmbeddedHtml(url, params));
    }
    
    public void testRenderWithDefaultDimensions()
    {
        expectedParam.put("width", "800px");
        expectedParam.put("height", "600px");
        
        assertEquals(message, googleCalenderRenderer.getEmbeddedHtml(url, params));
    }
}
