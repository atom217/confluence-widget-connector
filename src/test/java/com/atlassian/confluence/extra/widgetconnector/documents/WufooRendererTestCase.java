package com.atlassian.confluence.extra.widgetconnector.documents;

import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import junit.framework.TestCase;

public class WufooRendererTestCase extends TestCase
{
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/iframe.vm";
    
    private WufooRenderer wufooRenderer;
    
    private String url = "http://myuser.wufoo.com/12345/54321/";
    private String reportUrl = "http://myuser.wufoo.com/reports/54321/";
    private String embedUrl = "//myuser.wufoo.com/embed/54321/";
    private String reportEmbedUrl = "//myuser.wufoo.com/reports/54321/";
    private String message = "Done Executing";
    
    private Map<String, String> params, expectedParam;
    
    @Mock private VelocityRenderService velocityRenderService;
    
    @Override
    protected void setUp() throws Exception 
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        params = new HashMap<String, String>();
        expectedParam = new HashMap<String, String>();
        
        expectedParam.put("_template", VELOCITY_TEMPLATE);
        
        wufooRenderer = new WufooRenderer(velocityRenderService);
        
        when(velocityRenderService.render(embedUrl, expectedParam)).thenReturn(message);
    }
    
    public void testResultLinkContainsEmbedWufooLink()
    {
        assertEquals(embedUrl, wufooRenderer.getEmbedUrl(url));
    }

    public void testEmbedReport() {
        assertEquals(reportEmbedUrl, wufooRenderer.getEmbedUrl(reportUrl));
    }

    public void testRenderWithCustomDimensions()
    {
        params.put("width", "50%");
        params.put("height", "250px");
        
        expectedParam.put("width", "50%");
        expectedParam.put("height", "250px");
        
        assertEquals(message, wufooRenderer.getEmbeddedHtml(url, params));
    }
    
    public void testRenderWithDefaultDimensions()
    {
        expectedParam.put("width", "100%");
        expectedParam.put("height", "500px");
        
        assertEquals(message, wufooRenderer.getEmbeddedHtml(url, params));
    }
}
