package it.com.atlassian.confluence.extra.widgetconnector.chatter;

import it.com.atlassian.confluence.extra.widgetconnector.AbstractConfluencePluginWebTestCaseBase;

public class ChatterRendererTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    
    public void testFriendFeedWidget()
    {
        String username = "myname";
        String embedUrl = "//friendfeed.com/embed/widget/" + username + "?v=2&width=200";
        String friendFeedUrl = "http://friendfeed.com/" + username;
        
        final long pageId = createPage(spaceKey, "testFriendFeedWidget", "{widget:url=" + friendFeedUrl + "}");
        
        viewPageById(pageId);
        
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//script", "src"));
    }
}
