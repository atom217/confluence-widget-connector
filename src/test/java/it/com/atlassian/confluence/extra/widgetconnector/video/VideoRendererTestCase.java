package it.com.atlassian.confluence.extra.widgetconnector.video;

import it.com.atlassian.confluence.extra.widgetconnector.AbstractConfluencePluginWebTestCaseBase;

public class VideoRendererTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    private String width = "550";
    private String height = "450";
    private String flashVars = "$flashVars";
    
    public void testYoutubeVideoWidget()
    {
        String embedUrl = "//www.youtube.com/embed/dHi-ZcvFV_0?wmode=opaque";
        String url = "http://au.youtube.com/watch?v=dHi-ZcvFV_0&feature=rec-fresh";
        String cssStyle = "width: 550px; height: 450px";
        
        final long pageId = createPage(spaceKey, "testYouTubeVideoWidget", "{widget:url=" + url + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertYoutubeVideoContent(cssStyle, embedUrl);
    }

    public void testYoutubeVideoWidgetWithEmbeddedPlayerString()
    {
        String embedUrl = "//www.youtube.com/embed/0z9wAlHeOf4?wmode=opaque";
        String url = "http://www.youtube.com/watch?feature=player_embedded&v=0z9wAlHeOf4";
        String cssStyle = "width: 550px; height: 450px";

        final long pageId = createPage(spaceKey, "testYoutubeVideoWidgetWithEmbeddedPlayerString", "{widget:url=" + url +
                "|width=" + width + "|height=" + height + "}");

        viewPageById(pageId);

        assertYoutubeVideoContent(cssStyle, embedUrl);
    }

    public void testVimeoVideoWidget()
    {
        String embedUrl = "//vimeo.com/moogaloop.swf?clip_id=41159601&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1";
        String url = "http://www.vimeo.com/41159601";
        
        final long pageId = createPage(spaceKey, "testVimeoVideoWidget", "{widget:url=" + url + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertVideoContent(width, height, embedUrl, flashVars);
    }

    // CONFDEV-17523
    public void testVimeoVideoWidgetSetsWmode()
    {
        String embedUrl = "//vimeo.com/moogaloop.swf?clip_id=41159601&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1";
        String url = "http://www.vimeo.com/41159601";

        final long pageId = createPage(spaceKey, "testVimeoVideoWidget", "{widget:url=" + url +
                "|width=" + width + "|height=" + height + "}");

        viewPageById(pageId);

        assertEquals("opaque", getElementAttributByXPath("//div[@class='wiki-content']//object//param[@name='wmode']", "value"));
    }

    public void testMetacafeVideoWidget()
    {
        String embedUrl = "//www.metacafe.com/fplayer/1926002/funny_commercial_dirt_free.swf";
        String url = "http://www.metacafe.com/watch/1926002/funny_commercial_dirt_free/";
        
        final long pageId = createPage(spaceKey, "testMetacafeVideoWidget", "{widget:url=" + url + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertVideoContent(width, height, embedUrl, flashVars);
    }
    
    public void testGoogleVideoWidget()
    {
        String embedUrl = "//video.google.com/googleplayer.swf?docid=7134231252031614004&hl=en&fs=true";
        String url = "http://video.google.com/videoplay?docid=7134231252031614004&hl=en";
        
        final long pageId = createPage(spaceKey, "testGoogleVideoWidget", "{widget:url=" + url + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertVideoContent(width, height, embedUrl, flashVars);
    }
    
    public void testEpisodicVideoWidget()
    {
        String embedUrl = "//www.metacafe.com/fplayer/1926002/funny_commercial_dirt_free.swf";
        String url = "http://www.metacafe.com/watch/1926002/funny_commercial_dirt_free/";
        
        final long pageId = createPage(spaceKey, "testEpisodicVideoWidget", "{widget:url=" + url + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertVideoContent(width, height, embedUrl, flashVars);
    }

    public void testMySpaceVideoWidgetWithNewLink()
    {
        String embedUrl = "//myspace.com/play/video/glacier-creek-confluence-time-lapse-3376248";
        String url = "http://www.myspace.com/video/theflynn/glacier-creek-confluence-time-lapse/3376248";

        final long pageId = createPage(spaceKey, "testMySpaceVideoWidgetWithNewLink", "{widget:url=" + url +
                "|width=" + width + "|height=" + height + "}");

        viewPageById(pageId);

        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "src"));

        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "width"));

        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "height"));
    }

    public void testMySpaceVideoWidgetWithVeryOldLink()
    {
        String errorText = "This myspace URL is outdated, please update.";
        String url = "http://vids.myspace.com/index.cfm?fuseaction=vids.individual&amp;videoid=3376248&amp;searchid=20c789f6-1ae9-459a-bfec-75efcfc2847c";

        final long pageId = createPage(spaceKey, "testMySpaceVideoWidgetWithVeryOldLink", "{widget:url=" + url +
                "|width=" + width + "|height=" + height + "}");

        viewPageById(pageId);

        assertEquals(errorText, getElementTextByXPath("//div[@class='wiki-content']//div[@class='widget-error']//span[@class='widget-link']"));
    }

    public void testViddlerVideoWidget()
    {
        String embedUrl = "//www.metacafe.com/fplayer/1926002/funny_commercial_dirt_free.swf";
        String url = "http://www.metacafe.com/watch/1926002/funny_commercial_dirt_free/";
        
        final long pageId = createPage(spaceKey, "testViddlerVideoWidget", "{widget:url=" + url + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertVideoContent(width, height, embedUrl, flashVars);
    }
    
    public void testBlipVideoWidget()
    {
        String embedUrl = "//www.metacafe.com/fplayer/1926002/funny_commercial_dirt_free.swf";
        String url = "http://www.metacafe.com/watch/1926002/funny_commercial_dirt_free/";
        
        final long pageId = createPage(spaceKey, "testBlipVideoWidget", "{widget:url=" + url + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertVideoContent(width, height, embedUrl, flashVars);
    }
    
    public void testYahooVideoWidget()
    {
        String embedUrl = "//d.yimg.com/static.video.yahoo.com/yep/YV_YEP.swf?ver=2.2.34";
        String url = "http://video.yahoo.com/watch/4422504/11856052";
        
        final long pageId = createPage(spaceKey, "testYahooVideoWidget", "{widget:url=" + url + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertVideoContent(width, height, embedUrl, "id=11856052&vid=4422504&lang=en-us&intl=us&embed=1");
    }
    
    public void testTudouVideoWidget()
    {
        String embedUrl = "//tudou.com/v/5DDj1sfSxzs&hl=en&fs=1";
        String url = "http://www.tudou.com/v/5DDj1sfSxzs";
        
        final long pageId = createPage(spaceKey, "testTudouVideoWidget", "{widget:url=" + url + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertVideoContent(width, height, embedUrl, flashVars);
    }

//    TODO: Re-enable when https://ecosystem.atlassian.net/browse/WC-73 is fixed
//    public void testOoyalaVideoWidget()
//    {
//        String ooyalaUrl = "//player.ooyala.com";
//        String embedCode = "55dXMxMjoBhTgtutLWhR4aG6OklYZxak";
//        String url = "<script src=\"" + ooyalaUrl + "/player.js?embedCode=" + embedCode + "&width=712&deepLinkEmbedCode=55dXMxMjoBhTgtutLWhR4aG6OklYZxak%2CpxZ3QxMjrRNXoiEeAyVTKfMG-7Yv3vz8&height=400\"></script>";
//
//        final long pageId = createPage(spaceKey, "testOoyalaVideoWidget", "{widget:url=" + url +
//                "|width=" + width + "|height=" + height + "}");
//
//        viewPageById(pageId);
//
//        assertEquals(embedCode, getElementAttributByXPath("//div[@class='ooyala-embed-container']/fieldset/input[@name='embedCode']", "value"));
//    }

    private void assertVideoContent(String width, String height,
            String embedUrl, String flashVars)
    {
    	assertEquals("myFlashContent",
    			getElementAttributByXPath("//div[@class='wiki-content']//object", "id"));
    	
    	assertEquals("clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
    			getElementAttributByXPath("//div[@class='wiki-content']//object", "classid"));
    	
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//object", "width"));
        
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//object", "height"));
        
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//object//param[@name='movie']", "value"));
        
        assertEquals("application/x-shockwave-flash",
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "type"));
        
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "data"));
        
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "width"));
        
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "height"));
        
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object//param[@name='movie']", "value"));

    }
    
    private void assertYoutubeVideoContent(String cssStyle, String embedUrl)
    {
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe[@class='youtube-player']", "src"));
        assertEquals(cssStyle,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe[@class='youtube-player']", "style"));
        assertEquals("text/html",
                getElementAttributByXPath("//div[@class='wiki-content']//iframe[@class='youtube-player']", "type"));
    }
}
