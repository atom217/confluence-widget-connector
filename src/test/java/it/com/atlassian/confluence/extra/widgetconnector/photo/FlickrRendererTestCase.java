package it.com.atlassian.confluence.extra.widgetconnector.photo;

import it.com.atlassian.confluence.extra.widgetconnector.AbstractConfluencePluginWebTestCaseBase;

public class FlickrRendererTestCase  extends AbstractConfluencePluginWebTestCaseBase
{
    private String width = "500";
    private String height = "450";
    private String placeHolder = "//www.flickr.com/apps/slideshow/show.swf?v=61927";
    private String username = "atlassian";
    private String userId = "36981341@N00";
    
    public void testFlickrLinkWithTag()
    {
        String tags = "atlassian";
        String flickrUrl = "http://www.flickr.com/photos/tags/" + tags;
      
        String embedUrl = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2Ftags%2F" + tags + 
            "%2Fshow%2F&page_show_back_url=%2Fphotos%2Ftags%2F" + tags + "%2F&tags=" + tags + "&jump_to=&st" + tags + "_index=";
      
        final long pageId = createPage(spaceKey, "testFlickrLinkWithTag", 
            "{widget:url=" + flickrUrl +"|width=" + width + "|height=" + height + "}");
      
        viewPageById(pageId);
      
        assertFlickrContent(embedUrl);
    }

    public void testFlickrLinkWithUser()
    {
        String embedUrl = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2F" + username + "%2Fshow%2F&page_show_back_url=%2Fphotos%2F" + username + "%2F&user_id=" + userId + "&jump_to=";
        String flickrUrl = "http://www.flickr.com/photos/" + username;
        
        final long pageId = createPage(spaceKey, "testFlickrLinkWithUser", 
                "{widget:url=" + flickrUrl +"|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertFlickrContent(embedUrl);
    }
    
    public void testFlickrLinkWithSet()
    {
        String setId = "72157608657271078";
        String embedUrl = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2F" + username + "%2Fsets%2F" + setId +"%2Fshow%2F&page_show_back_url=%2Fphotos%2F" + username + "%2Fsets%2F" + setId + "%2F&set_id=" + setId + "&jump_to=";
        String flickrUrl = "http://www.flickr.com/photos/" + username + "/sets/72157608657271078";
        
        final long pageId = createPage(spaceKey, "testFlickrLinkWithSet", 
                "{widget:url=" + flickrUrl +"|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertFlickrContent(embedUrl);
    }
    
    public void testFlickrLinkWithPhoto()
    {
        String photoId = "3003538919";
        String embedUrl = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2F" + username + "%2Fshow%2Fwith%2F" + photoId + "%2F&page_show_back_url=%2Fphotos%2F" + username + "%2Fwith%2F" + photoId + "%2F&user_id=" + userId + "&jump_to=" + photoId;
        String flickrUrl = "http://www.flickr.com/photos/" + username + "/3003538919/";
        
        final long pageId = createPage(spaceKey, "testFlickrLinkWithPhoto", 
                "{widget:url=" + flickrUrl +"|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertFlickrContent(embedUrl);
    }

    // CONFDEV-17523
    public void testFlickrSetsWmode()
    {
        String flickrUrl = "http://www.flickr.com/photos/" + username + "/sets/72157608657271078";

        final long pageId = createPage(spaceKey, "testFlickrLinkWithSet",
                "{widget:url=" + flickrUrl +"|width=" + width + "|height=" + height + "}");

        viewPageById(pageId);

        assertEquals("opaque", getElementAttributByXPath("//div[@class='wiki-content']//object//param[@name='wmode']", "value"));
    }

    private void assertFlickrContent(String flashVars)
    {
        assertElementPresentByXPath("//div[@class='wiki-content']//object[@type='application/x-shockwave-flash']");
        assertEquals(width,
              getElementAttributByXPath("//div[@class='wiki-content']//object[@type='application/x-shockwave-flash']", "width"));
        assertEquals(height,
              getElementAttributByXPath("//div[@class='wiki-content']//object[@type='application/x-shockwave-flash']", "height"));
        assertEquals(flashVars,
              getElementAttributByXPath("//div[@class='wiki-content']//object[@type='application/x-shockwave-flash']//param[@name='flashvars']", "value"));
      
        assertEquals(placeHolder,
              getElementAttributByXPath("//div[@class='wiki-content']//object[@type='application/x-shockwave-flash']//param[@name='movie']", "value"));

        assertEquals(flashVars,
              getElementAttributByXPath("//div[@class='wiki-content']//object[@type='application/x-shockwave-flash']//embed[@type='application/x-shockwave-flash']", "flashvars"));
        assertEquals(placeHolder,
              getElementAttributByXPath("//div[@class='wiki-content']//object[@type='application/x-shockwave-flash']//embed[@type='application/x-shockwave-flash']", "src"));
        assertEquals(width,
              getElementAttributByXPath("//div[@class='wiki-content']//object[@type='application/x-shockwave-flash']//embed[@type='application/x-shockwave-flash']", "width"));
        assertEquals(height,
              getElementAttributByXPath("//div[@class='wiki-content']//object[@type='application/x-shockwave-flash']//embed[@type='application/x-shockwave-flash']", "height"));
    }
}
