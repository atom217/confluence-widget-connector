package it.com.atlassian.confluence.extra.widgetconnector.documents;

import it.com.atlassian.confluence.extra.widgetconnector.AbstractConfluencePluginWebTestCaseBase;

public class DocumentsRendererTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    private String width = "550";
    private String height = "450";
    
    public void testSlideRocketWidget()
    {
        String protocol = "http";
        String id = "id=132f9db7-b0fb-4f51-b974-36652774971b";
        String slideRocketUrl = "//app.sliderocket.com/app/FullPlayer.aspx?" + id;
        
        final long pageId = createPage(spaceKey, "testSlideRocketWidget", "{widget:url=" + protocol + ":" + slideRocketUrl +
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertEquals(slideRocketUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "src"));
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "width"));
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "height"));
    }
    
    public void testSlideShareWidget()
    {
        String documentId = "jira-greenhopper-1226081764877776-8";
        String embedUrl = "//static.slideshare.net/swf/ssplayer2.swf?doc=" + documentId;
        String slideShareUrl = "http://www.slideshare.net/jleyser/using-jira-greenhopper-for-agile-development-presentation";
        
        final long pageId = createPage(spaceKey, "testSlideShareWidget", "{widget:url=" + slideShareUrl + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertEquals("myFlashContent",
    			getElementAttributByXPath("//div[@class='wiki-content']//object", "id"));
        assertEquals("clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
    			getElementAttributByXPath("//div[@class='wiki-content']//object", "classid"));
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//object", "width"));
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//object", "height"));
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//object//param[@name='movie']", "value"));
        
        assertEquals("application/x-shockwave-flash",
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "type"));
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "data"));
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "width"));
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "height"));
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object//param[@name='movie']", "value"));
    }
    
    public void testScribdWidget()
    {
        String width = "400px";
        String height = "600px";
        
        String docId = "116271429";
        String accessKey = "key-d5i0b83gv3joq8gld4d";
        String embedUrl = "//documents.scribd.com/ScribdViewer.swf?document_id=" + docId + "&access_key=" + accessKey + "&version=1";
        String scribdUrl = "http://www.scribd.com/doc/" + docId + "/Green-Lantern-Corps-Issue-15-Exclusive-Preview?access_key=" + accessKey +"&amp;version=1";
        String codeBaseUrl = "https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0";
        
        final long pageId = createPage(spaceKey, "testScribdWidget", "{widget:url=" + scribdUrl + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertEquals(codeBaseUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//object", "codebase"));
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//object", "width"));
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//object", "height"));
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//object//param[@name='movie']", "value"));
        
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//object//embed", "width"));
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//object//embed", "height"));
    }
    
    public void testGoogleDocsWidget()
    {
        String docId = "1wFSW4MAv8bDNUaylGKtsKw5RJ-UD3nmDtFlI9wngW28";
        String embedUrl = "//docs.google.com/presentation/d/" + docId + "/embed";
        String googleDocsUrl = "http://docs.google.com/presentation/d/" + docId + "/pub?embedded=true";
        
        final long pageId = createPage(spaceKey, "testGoogleDocsWidget", "{widget:url=" + googleDocsUrl + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "src"));
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "width"));
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "height"));
    }
    
    public void testGoogleCalendarWidget()
    {
        // Only registered email with calendar is valid
        String calId = "dummydummy002%40gmail.com";
        String googleCalendarUrl = "http://www.google.com/calendar/embed?src=" + calId;
        
        final long pageId = createPage(spaceKey, "testGoogleCalendarWidget", "{widget:url=" + googleCalendarUrl + 
              "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "width"));
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "height"));
        assertEquals("0",
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "frameborder"));
        assertEquals(googleCalendarUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "src"));
    }
    
    public void testDabbleDbWidget()
    {
        String idCode = "KHzdBpjU";
        String embedUrl = "https://widgetconnectorx.dabbledb.com/page/widgetconnectorx/" + idCode + "?embed=true";
        String dabbleDbUrl = "https://widgetconnectorx.dabbledb.com/page/widgetconnectorx/" + idCode;
        
        final long pageId = createPage(spaceKey, "testDabbleDbWidget", "{widget:url=" + dabbleDbUrl + 
              "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "src"));
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "width"));
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//iframe", "height"));
    }
    
    public void testShareAcrobatWidget()
    {
        String acrobatPlaceHolderUrl = "https://share.acrobat.com/adc/flex/mpt.swf";
        String idCode = "24eaa078-2d86-4de2-9d37-2a5572203a46";
        String flashVars = "docId=" + idCode;
        String shareAcrobatUrl = "https://share.acrobat.com/adc/document.do?docid=" + idCode;
        
        final long pageId = createPage(spaceKey, "testShareAcrobatWidget", "{widget:url=" + shareAcrobatUrl + 
                "|width=" + width + "|height=" + height + "}");
        
        viewPageById(pageId);
        
        assertEquals("myFlashContent",
    			getElementAttributByXPath("//div[@class='wiki-content']//object", "id"));
        assertEquals("clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
    			getElementAttributByXPath("//div[@class='wiki-content']//object", "classid"));
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//object", "width"));
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//object", "height"));
        assertEquals(acrobatPlaceHolderUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//object//param[@name='movie']", "value"));
        assertEquals(flashVars,
                getElementAttributByXPath("//div[@class='wiki-content']//object//param[@name='flashVars']", "value"));
        
        assertEquals("application/x-shockwave-flash",
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "type"));
        assertEquals(acrobatPlaceHolderUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "data"));
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "width"));
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object", "height"));
        assertEquals(acrobatPlaceHolderUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//object//object//param[@name='movie']", "value"));
    }
}
