package it.com.atlassian.confluence.extra.widgetconnector;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;

public class AbstractConfluencePluginWebTestCaseBase extends AbstractConfluencePluginWebTestCase
{
    public static final String spaceKey = "ds";

    protected long createPage(String spaceKey, String title, String content)
    {
        return createPage(spaceKey, 0, title, content, new ArrayList());
    }
    
    protected long createPage(String spaceKey, long parentId, String title, String content, List labels)
    {
        PageHelper helper = getPageHelper();

        helper.setSpaceKey(spaceKey);
        helper.setParentId(parentId);
        helper.setTitle(title);
        helper.setContent(content);
        helper.setCreationDate(new Date());
        helper.setLabels(labels);
        assertTrue(helper.create());

        // return the generated id for the new page
        return helper.getId();
    }
    
    protected void viewPageById(long entityId)
    {
        gotoPage("/pages/viewpage.action?pageId=" + entityId);
    }
}
