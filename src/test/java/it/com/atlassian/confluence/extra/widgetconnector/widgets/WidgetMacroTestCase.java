package it.com.atlassian.confluence.extra.widgetconnector.widgets;

import it.com.atlassian.confluence.extra.widgetconnector.AbstractConfluencePluginWebTestCaseBase;
import junit.framework.AssertionFailedError;

public class WidgetMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    public void testInvalidUrlWidget()
    {
        String url = "http://";
        gotoPage("/display/TST/testInvalidUrlWidget");

        assertElementPresentByXPath("//div[@class='wiki-content']//div[@class='widget-error']");

        assertEquals(url, getElementTextByXPath(
            "//div[@class='wiki-content']//div[@class='widget-error']//span[@class='widget-link']"));
    }
    
    public void testInvalidUrlIsHtmlEncoded()
    {
        String url = "url<with>brackets'and\"quotes"; // if it doesn't match a URL pattern, the whole thing is displayed
        gotoPage("/display/TST/testInvalidUrlIsHtmlEncoded");

        assertElementPresentByXPath("//div[@class='wiki-content']//div[@class='widget-error']");

        assertEquals(url, getElementTextByXPath(
            "//div[@class='wiki-content']//div[@class='widget-error']//span[@class='widget-link']"));
    }

    public void testJavaScriptUrlWidget()
    {
        String url = "javascript:alert(1)";
        gotoPage("/display/TST/testJavaScriptUrlWidget");

        assertElementPresentByXPath("//div[@class='wiki-content']//div[@class='widget-error']");

        assertEquals(url, getElementTextByXPath(
            "//div[@class='wiki-content']//div[@class='widget-error']//span[@class='widget-link']"));

        try
        {
            // JavaScript URL should not be linked
            assertElementNotPresentByXPath("//a[contains(@href, '" + url + "')]");
        }
        catch (AssertionFailedError e)
        {
            throw new AssertionFailedError(e.getMessage() + ", page source: " + getPageSource());
        }
    }

    public void testGoogleGadgetWidget()
    {
        String width = "250";
        String height = "350";
        String gadgetXmlUrl = "http://www.gadzi.com/gadgets/monkey.xml";
        String url = "http://www.google.com/ig/adde?synd=open&source=ggyp&moduleurl=" + gadgetXmlUrl + "|width=" + width + "|height=" + height;
        
        String expectedScriptSource = "https://www.gmodules.com/ig/ifr?url=" + gadgetXmlUrl + "&up_vsize=240px&synd=open&w="+
            width+"&h="+height+"&border=%23ffffff%7C3px%2C1px+solid+%23999999&output=js";
        
        final long pageId = createPage(spaceKey, "testGoogleMonkeyVirtualPetWidget", "{widget:url=" + url +"}");
        
        viewPageById(pageId);
        
        assertElementPresentByXPath("//div[@class='wiki-content']//div[@class='google-gadget']//script");
        
        assertEquals(expectedScriptSource,
                getElementAttributByXPath("//div[@class='wiki-content']//div[@class='google-gadget']//script", "src"));
    }
    
    public void testBackTypeWidgetWithUsername()
    {
        String username = "flamehaze";
        String embedUrl = "//widgets.backtype.com/" + username;
        String url = "http://www.backtype.com/" + username;
        
        final long pageId = createPage(spaceKey, "testBackTypeWidgetWithUsername", "{widget:url=" + url +"}");
        
        viewPageById(pageId);
        
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//script", "src"));
    }
    
    public void testBackTypeWidgetWithSharedUrl()
    {
        String username = "flamehaze";
        String embedUrl = "//widgets.backtype.com/" + username + "/shared";
        String url = "http://www.backtype.com/" + username + "/shared";
        
        final long pageId = createPage(spaceKey, "testBackTypeWidgetWithSharedUrl", "{widget:url=" + url +"}");
        
        viewPageById(pageId);
        
        assertEquals(embedUrl,
                getElementAttributByXPath("//div[@class='wiki-content']//script", "src"));
    }
    
    public void testWidgetBox()
    {
        String embedUrl = "//cdn.widgetserver.com/syndication/subscriber/InsertWidget.js";
        String idCode = "8e55feb2-51b3-4604-97f4-7bce293380ce";
        String url = "http://widgetbox.com/confluence/" + idCode;
        
        final long pageId = createPage(spaceKey, "testWidgetBox", "{widget:url=" + url +"}");
        
        viewPageById(pageId);
        assertEquals(embedUrl, 
                getElementAttributByXPath("//div[@class='wiki-content']//script[1]", "src"));
        assertElementPresentByXPath("//div[@class='wiki-content']//script[2]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']//script[3]");
    }
}
