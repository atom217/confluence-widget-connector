;(function($) {
    var Ooyala = {};

    Ooyala.getParameter = function($ooyalaContainer, parameterName) {
        return $("fieldset input[name='" + parameterName + "']", $ooyalaContainer).val();
    };

    Ooyala.init = function($ooyalaContainer) {
        OO.Player.create($(".ooyala-embed-content", $ooyalaContainer).attr("id"), Ooyala.getParameter($ooyalaContainer, "embedCode")).play();
    };

    $(function() {
        var brandingId = $(".ooyala-embed-container:first fieldset input[name='brandingId']").val();

        if (brandingId)
            $.getScript("//player.ooyala.com/v3/" + brandingId, function() {
                $(".ooyala-embed-container").each(function() {
                    Ooyala.init($(this));
                });
            })
    });

})(jQuery);