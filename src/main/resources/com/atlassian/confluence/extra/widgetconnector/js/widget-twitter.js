
;(function($) {
    var removeTwitterWidgetPlaceholderElements = function() {
        $(".twitter-widget").each(function() {
            $(".widget-placeholder", this).removeClass("hidden");
            $(".aui-message", this).remove();
        });
    };

    if (window.ConfluenceMobile) {
        ConfluenceMobile.contentEventAggregator.on("displayed", function() {
            // Mostly copied from Twitter.
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                if (!d.getElementById(id)) {
                    js = d.createElement(s);
                    js.id = id;
                    js.src = p + "://platform.twitter.com/widgets.js";
                    js.onload = function() {
                        removeTwitterWidgetPlaceholderElements();
                    };
                    fjs.parentNode.insertBefore(js, fjs);
                }
            })(document, "script", "twitter-wjs");

        });
    } else {
        $(function($) {
            $.getScript("//platform.twitter.com/widgets.js", function() {
                removeTwitterWidgetPlaceholderElements();
            });
        });
    }

})(window.ConfluenceMobile ? Zepto : AJS.$);