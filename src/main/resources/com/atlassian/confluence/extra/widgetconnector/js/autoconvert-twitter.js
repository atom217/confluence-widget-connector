(function() {
AJS.bind("init.rte", function() {
    var singleStatusTweetRegex = /^\/(.*?)\/status\/([^/]+\/?$)/,
        singleStatusesTweetRegex = /^\/(.*?)\/statuses\/([^/]+\/?$)/,

        pasteHandler = function(uri, node, done) {
            var match, newUrl, macro;

            if (uri.host.match(/^twitter.com/)) {
                if (match = decodeURI(uri.path).match(singleStatusTweetRegex)) {
                    newUrl = uri.protocol + "://twitter.com/" + match[1] + "/status/" + match[2];
                } else if (match = decodeURI(uri.path).match(singleStatusesTweetRegex)) {
                    newUrl = uri.protocol + "://twitter.com/" + match[1] + "/statuses/" + match[2];
                }

                if (newUrl) {
                    macro = {name:'widget', params: {url: newUrl}};
                    tinymce.plugins.Autoconvert.convertMacroToDom(macro, done, done);
                }
                else {
                    done();
                }
            }
            else {
                done();
            }
        };
    tinymce.plugins.Autoconvert.autoConvert.addHandler(pasteHandler);
});

})();
