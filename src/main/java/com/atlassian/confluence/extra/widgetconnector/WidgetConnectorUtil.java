package com.atlassian.confluence.extra.widgetconnector;
import com.atlassian.confluence.extra.widgetconnector.services.DefaultPlaceholderService;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.pages.thumbnail.Dimensions;
import com.atlassian.confluence.util.GeneralUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;


public class WidgetConnectorUtil
{
    private static final Logger log = LoggerFactory.getLogger(WidgetConnectorUtil.class);

    public static String cleanupUrl(String url)
    {
        if (url.indexOf('(') > 0)
            url = url.replaceAll("\\(", "%28");

        if (url.indexOf(')') > 0)
            url = url.replaceAll("\\)", "%29");

        if (url.indexOf("&amp;") > 0)
            url = url.replaceAll("&amp;", "&");

        return GeneralUtil.htmlEncode(url);
    }

    public static ImagePlaceholder generateDefaultImagePlaceholder(String baseUrl)
    {
        if(baseUrl == null || baseUrl.isEmpty())
        {
            baseUrl = "Widget Connector";
        }
        Dimensions dimension = new Dimensions(300, 225);
        String command = String.format("%s?%s=%s&%s=%d&%s=%d",
                DefaultPlaceholderService.PLACEHOLDER_SERVLET,
                DefaultPlaceholderService.PARAM_THUMB_URL, baseUrl,
                DefaultPlaceholderService.PARAM_WIDTH, dimension.getWidth(),
                DefaultPlaceholderService.PARAM_HEIGHT, dimension.getHeight());
        return new DefaultImagePlaceholder(command, dimension, true);
    }

    public static boolean isURLMatch(String url, String matchUrl)
    {
        try
        {
            final String host = new URI(url).getHost();
            return host != null && !host.isEmpty()
                    ? url.contains(matchUrl)
                    : false;
        }
        catch (URISyntaxException e)
        {
            log.error("Invalid URL [" + url + "] provided - " + e.getMessage());
        }
        return false;
    }
}
