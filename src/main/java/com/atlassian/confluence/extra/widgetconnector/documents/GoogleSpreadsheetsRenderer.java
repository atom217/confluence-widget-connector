package com.atlassian.confluence.extra.widgetconnector.documents;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;

public class GoogleSpreadsheetsRenderer implements WidgetRenderer
{
    private static final Pattern PATTERN = Pattern.compile("key=([^&]+)");
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/iframe.vm";
    private static final String DEFAULT_WIDTH = "720px";
    private static final String DEFAULT_HEIGHT = "360px";
    private VelocityRenderService velocityRenderService;
    private static final String[] MATCH_URLs = {"docs.google.com/.*spreadsheet", "spreadsheets.google.com"};
    private static final String WIDGET_PARAM = "widget=";
    private static final String ELEMENT_PARAM = "element=";
    private static final String OUTPUT_PARAM = "output=";

    public GoogleSpreadsheetsRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        String key;
        String embedUrl = "//docs.google.com/spreadsheet/pub?";

        Matcher m = PATTERN.matcher(url);
        if (m.find())
        {
            key = m.group(1);
            embedUrl += "key=" + key;
        }
        else {
            return null;
        }
        embedUrl += "&output=html&widget=true&element=true&gid=0";

        return embedUrl;
    }

    public boolean matches(String url)
    {
        for (String matchUrl : MATCH_URLs)
        {
            Pattern pattern = Pattern.compile(matchUrl);
            Matcher matcher = pattern.matcher(url);
            if (matcher.find())
            {
                return true;
            }
        }
        return false;
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        if (!params.containsKey(VelocityRenderService.WIDTH_PARAM))
            params.put(VelocityRenderService.WIDTH_PARAM, DEFAULT_WIDTH);
        if (!params.containsKey(VelocityRenderService.HEIGHT_PARAM))
            params.put(VelocityRenderService.HEIGHT_PARAM, DEFAULT_HEIGHT);

        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
