package com.atlassian.confluence.extra.widgetconnector.services;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.http.ConfluenceHttpParameters;
import com.atlassian.confluence.util.http.HttpResponse;
import com.atlassian.confluence.util.http.HttpRetrievalService;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.methods.HeadMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DefaultHttpRetrievalEmbedService implements HttpRetrievalEmbedService
{
    private static final Logger log = Logger.getLogger(DefaultHttpRetrievalEmbedService.class);

    private final SettingsManager settingsManager;

    private final CacheManager cacheManager;

    private final HttpRetrievalService httpRetrievalService;

    private final HttpConnectionManager httpConnectionManager;

    public DefaultHttpRetrievalEmbedService(SettingsManager settingsManager, CacheManager cacheManager, HttpRetrievalService httpRetrievalService)
    {
        this.settingsManager = settingsManager;
        this.cacheManager = cacheManager;
        this.httpRetrievalService = httpRetrievalService;

        httpConnectionManager = new SimpleHttpConnectionManager();
    }

    public String getEmbedData(String url, Pattern pattern, String cacheName)
    {
        String embedId;
        Cache cache = getCache(cacheName);
        if (cache != null && cache.get(url) != null)
            return (String) cache.get(url);

        HttpResponse response = getResponse(url);

        String line = "";
        try
        {
            if (response == null)
                return null;
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getResponse()));
            Matcher m;
            while ((line = br.readLine()) != null)
            {
                m = pattern.matcher(line);
                if (m.find())
                {
                    br.close();
                    embedId = m.group(1);
                    getCache(cacheName).put(url, embedId);
                    return embedId;
                }
            }
            br.close();
        }
        catch (IOException e)
        {
            log.error("An error occured parsing the response from: " + url);
        }
        finally
        {
            if (response != null)
                response.finish();
        }
        return null;
    }

    private HttpResponse getResponse(String url)
    {
        HttpResponse response = null;
        try
        {
            response = httpRetrievalService.get(url);
            // Todo pass response to frontend
            if (response.isNotFound())
            {
                log.error("URL not found: " + url);
                return null;
            }
            if (response.isNotPermitted())
            {
                log.error("URL not permitted: " + url);
                return null;
            }
            if (response.isFailed())
            {
                log.error("Failed to retrieve data from URL: " + url);
                return null;
            }
        }
        catch (IOException e)
        {
            log.error("Unable to retrieve " + url + ": " + e.getMessage(), e);
        }
        return response;
    }

    private Cache getCache(String cacheName)
    {
        return cacheManager.getCache(cacheName);
    }

    @Override
    public String getNewLocation(String oldUrl)
    {
        HttpMethod headMethod = null;

        try
        {
            HttpClient client = getClient();
            (headMethod = new HeadMethod(oldUrl)).setFollowRedirects(false);

            if (HttpServletResponse.SC_MOVED_PERMANENTLY == client.executeMethod(headMethod))
                return headMethod.getResponseHeader("Location").getValue();
        }
        catch (IOException ioError)
        {
            log.error(String.format("IO error while trying to make a HEAD request to %s", oldUrl), ioError);
        }
        finally
        {
            if (null != headMethod)
                headMethod.releaseConnection();
        }

        return oldUrl;
    }

    private HttpClient getClient()
    {
        HttpConnectionManagerParams httpConnectionManagerParams = httpConnectionManager.getParams();
        if (null == httpConnectionManagerParams)
            httpConnectionManagerParams = new HttpConnectionManagerParams();

        ConfluenceHttpParameters confluenceHttpParameters = settingsManager.getGlobalSettings().getConfluenceHttpParameters();

        httpConnectionManagerParams.setConnectionTimeout(confluenceHttpParameters.getConnectionTimeout());
        httpConnectionManagerParams.setSoTimeout(confluenceHttpParameters.getSocketTimeout());

        return new HttpClient(httpConnectionManager);

    }
}
