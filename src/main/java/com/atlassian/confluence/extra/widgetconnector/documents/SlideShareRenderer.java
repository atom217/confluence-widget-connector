package com.atlassian.confluence.extra.widgetconnector.documents;

import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.regex.Pattern;


public class SlideShareRenderer implements WidgetRenderer
{
    private HttpRetrievalEmbedService httpRetrievalEmbedService;

    public static final String MATCH_URL = "slideshare.net";
    public static final Pattern PATTERN = Pattern.compile("\"doc\":\"([^\"]+)");
    private static final String DEFAULT_WIDTH = "425";
    private static final String DEFAULT_HEIGHT = "355";
    private VelocityRenderService velocityRenderService;


    public SlideShareRenderer(HttpRetrievalEmbedService httpRetrievalEmbedService, VelocityRenderService velocityRenderService)
    {
        this.httpRetrievalEmbedService = httpRetrievalEmbedService;
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {

        String embedParameter = httpRetrievalEmbedService.getEmbedData(url, PATTERN, this.getClass().getName());
        if (StringUtils.isNotEmpty(embedParameter))
            return "//static.slideshare.net/swf/ssplayer2.swf?doc=" + embedParameter;

        return null;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        if (!params.containsKey(VelocityRenderService.WIDTH_PARAM))
            params.put(VelocityRenderService.WIDTH_PARAM, DEFAULT_WIDTH);
        if (!params.containsKey(VelocityRenderService.HEIGHT_PARAM))
            params.put(VelocityRenderService.HEIGHT_PARAM, DEFAULT_HEIGHT);

        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
