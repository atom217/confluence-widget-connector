package com.atlassian.confluence.extra.widgetconnector.services;

import org.apache.commons.lang.StringUtils;

import java.util.Map;

import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

/**
 * Extend the AbstractVelocityRenderer if you don't want to handle the velocity rendering part in your
 * WidgetRenderer. A default embed tag template that supports width and height parameters will be used by default.
 *
 * You can render a custom template if you pass it via the params map using the VelocityRenderService.TEMPLATE_PARAM key.
 *
 */
public class DefaultVelocityRenderService implements VelocityRenderService
{


    // only for internal use to allow classes with different templates but same properties
    // to extend the AbstractVelocityRenderer

    private final String DEFAULT_WIDTH = "400";
    private final String DEFAULT_HEIGHT = "300";
    private final String DEFAULT_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/embed.vm";

    public String render(String url, Map<String, String> params)
    {
        String width = params.get(VelocityRenderService.WIDTH_PARAM);
        String height = params.get(VelocityRenderService.HEIGHT_PARAM);
        String template = params.get(VelocityRenderService.TEMPLATE_PARAM);

        if (StringUtils.isEmpty(template))
            template = DEFAULT_TEMPLATE;

        if (StringUtils.isEmpty(url))
            return null;

        Map<String, Object> contextMap = getDefaultVelocityContext();

        for (Map.Entry<String, String> entry : params.entrySet())
        {
            if (entry.getKey().contentEquals("tweetHtml"))
                contextMap.put(entry.getKey(), entry.getValue());
            else
                contextMap.put(entry.getKey(), GeneralUtil.htmlEncode(entry.getValue()));
        }

        contextMap.put("urlHtml", GeneralUtil.htmlEncode(url));

        if (StringUtils.isNotEmpty(width))
        {
            contextMap.put("width", GeneralUtil.htmlEncode(width));
        }
        else
        {
            contextMap.put("width", DEFAULT_WIDTH);
        }

        if (StringUtils.isNotEmpty(height))
        {
            contextMap.put("height", GeneralUtil.htmlEncode(height));
        }
        else
        {
            contextMap.put("height", DEFAULT_HEIGHT);
        }

        return getRenderedTemplate(template, contextMap);
    }

    protected String getRenderedTemplate(String template,
            Map<String, Object> contextMap)
    {
        return VelocityUtils.getRenderedTemplate(template, contextMap);
    }

    protected Map<String, Object> getDefaultVelocityContext()
    {
        return MacroUtils.defaultVelocityContext();
    }
}
