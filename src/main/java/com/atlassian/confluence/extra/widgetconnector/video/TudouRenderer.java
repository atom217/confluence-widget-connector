package com.atlassian.confluence.extra.widgetconnector.video;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;


public class TudouRenderer implements WidgetRenderer
{
    private static final String MATCH_URL = "tudou";
    private static final Pattern PATTERN = Pattern.compile("v/([^/]+)");
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/embed.vm";
    private VelocityRenderService velocityRenderService;
    
    public TudouRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        Matcher m = PATTERN.matcher(url);

        String videoId = "";

        if (m.find())
        {
            videoId = m.group(1);
        }

        return "//tudou.com/v/" + videoId + "&hl=en&fs=1";
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(getEmbedUrl(url), params);
    }

}