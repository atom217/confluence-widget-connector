package com.atlassian.confluence.extra.widgetconnector.chatter;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.http.HttpResponse;
import com.atlassian.confluence.util.http.HttpRetrievalService;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.v2.RenderUtils;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTimeConstants;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TwitterRenderer implements WidgetRenderer
{
    private static final Logger LOG = LoggerFactory.getLogger(TwitterRenderer.class);

    private static final String SINGLE_TWEET_CACHE_KEY = "com.atlassian.confluence.extra.widgetconnector.chatter.TwitterRenderer";

    private static final long SINGLE_TWEET_RETRIEVAL_FAILURE_DELAY = Long.getLong("com.atlassian.confluence.extra.widgetconnector.chatter.TwitterRenderer.singletweet.failure.delay", DateTimeConstants.MILLIS_PER_MINUTE);

    private static final Pattern SINGLE_TWEET_PATTERN = Pattern.compile("^(https?)://twitter\\.com/(.*?)/status(es)??/(\\d+)+/?$");

    private static final Pattern WIDGET_PATTERN = Pattern.compile("(https?)://twitter\\.com/settings/widgets/(\\d+).*$");

    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/twitter.vm";
    private static final String SINGLE_TWEET_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/tweet.vm";
    private static final String DEFAULT_WIDTH = "250";
    private static final String DEFAULT_HEIGHT = "300";

    private final I18NBeanFactory i18NBeanFactory;

    private final LocaleManager localeManager;

    private final CacheManager cacheManager;

    private final WebResourceManager webResourceManager;

    private final VelocityRenderService velocityRenderService;

    private final HttpRetrievalService httpRetrievalService;

    public TwitterRenderer(I18NBeanFactory i18NBeanFactory, LocaleManager localeManager, CacheManager cacheManager, WebResourceManager webResourceManager, VelocityRenderService velocityRenderService, HttpRetrievalService httpRetrievalService)
    {
        this.i18NBeanFactory = i18NBeanFactory;
        this.localeManager = localeManager;
        this.cacheManager = cacheManager;
        this.webResourceManager = webResourceManager;
        this.velocityRenderService = velocityRenderService;
        this.httpRetrievalService = httpRetrievalService;
    }

    public Map<String, String> getParameters(String url, Map<String, String> params)
    {
        final Matcher singleTweetMatcher = SINGLE_TWEET_PATTERN.matcher(url);
        final Matcher widgetMatcher;

        if (singleTweetMatcher.matches())
        {
            populateDefaultDimensionParams(params);
            params.put(VelocityRenderService.TEMPLATE_PARAM, SINGLE_TWEET_TEMPLATE);
            params.put("tweetHtml", getTweetHtml(singleTweetMatcher.group(1), singleTweetMatcher.group(2), singleTweetMatcher.group(4)));
        }
        else if ((widgetMatcher = WIDGET_PATTERN.matcher(url)).matches())
        {
            populateDefaultDimensionParams(params);

            params.put("widgetId", widgetMatcher.group(2));
            params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        }

        return params;
    }

    private void populateDefaultDimensionParams(Map<String, String> params)
    {
        if (!params.containsKey(VelocityRenderService.WIDTH_PARAM))
            params.put(VelocityRenderService.WIDTH_PARAM, String.valueOf(-1));
        if (!params.containsKey(VelocityRenderService.HEIGHT_PARAM))
            params.put(VelocityRenderService.HEIGHT_PARAM, String.valueOf(-1));
    }

    private String getTweetHtml(String protocol, String twitterUserName, String statusId)
    {
        Cache singleTweetsCache = getSingleTweetsCache();
        TweetRetrievalResult tweetRetrievalResult = getTweetRetrievalResultFromCache(statusId);

        if (tweetRetrievalResult == null)
        {
            singleTweetsCache.put(statusId, tweetRetrievalResult = getTweetHtmlFromTwitter(protocol, statusId));

            if (tweetRetrievalResult.successful)
                return tweetRetrievalResult.tweetMarkup;
            else
                return getErrorMessageMarkup(protocol, twitterUserName, statusId);
        }
        else
        {
            // Retrieved from cache.
            if (tweetRetrievalResult.successful)
            {
                return tweetRetrievalResult.tweetMarkup;
            }
            else if (tweetRetrievalResult.isFailedStatusTimedOut())
            {
                singleTweetsCache.remove(statusId);
                return getTweetHtml(protocol, twitterUserName, statusId);
            }
            else
            {
                return getErrorMessageMarkup(protocol, twitterUserName, statusId);
            }
        }
    }

    private String getErrorMessageMarkup(String protocol, String twitterUserName, String statusId)
    {
        return RenderUtils.blockError(
                getText(
                        "com.atlassian.confluence.extra.widgetconnector.twitter.single.error",
                        protocol + String.format("://twitter.com/%s/status/%s", twitterUserName, statusId),
                        SINGLE_TWEET_RETRIEVAL_FAILURE_DELAY / DateTimeConstants.MILLIS_PER_MINUTE
                ),
                ""
        );
    }

    private TweetRetrievalResult getTweetRetrievalResultFromCache(String statusId)
    {
        Cache singleTweetsCache = getSingleTweetsCache();

        try
        {
            return (TweetRetrievalResult) singleTweetsCache.get(statusId);
        }
        catch (ClassCastException pluginUpgraded)
        {
            LOG.debug("Removed cached Tweet retrieval result by a previously installed version of the Widget Connector Plugin.", pluginUpgraded);
            singleTweetsCache.remove(statusId);
            return null;
        }
    }

    private Cache getSingleTweetsCache()
    {
        return cacheManager.getCache(SINGLE_TWEET_CACHE_KEY);
    }

    private String getText(String key, Object ... substitutions)
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser())).getText(key, substitutions);
    }

    private TweetRetrievalResult getTweetHtmlFromTwitter(String protocol, String statusId)
    {
        try
        {
            HttpResponse response = httpRetrievalService.get(String.format(
                    "%s://api.twitter.com/1/statuses/oembed.json?id=%s&omit_script=true&lang=%s",
                    protocol,
                    statusId,
                    GeneralUtil.urlEncode(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()).getLanguage())
            ));

            if (!(response.isFailed() || response.isNotFound() || response.isNotPermitted()))
            {
                InputStream jsonInput = null;
                try
                {
                    return newSuccessfulResult(new JSONObject(IOUtils.toString(jsonInput = response.getResponse(), "UTF-8")).getString("html"));
                }
                finally
                {
                    IOUtils.closeQuietly(jsonInput);
                }
            }
            else
            {
                return newFailureResult(response.getStatusMessage());
            }
        }
        catch (JSONException invalidResponse)
        {
            LOG.error("Invalid JSON returned by Twitter", invalidResponse);
            return newFailureResult(invalidResponse.getMessage());
        }
        catch (IOException retrieveTweetError)
        {
            LOG.error("Unable to read response from Twitter", retrieveTweetError);
            return newFailureResult(retrieveTweetError.getMessage());
        }
    }

    private TweetRetrievalResult newFailureResult(String failureMessage)
    {
        return new TweetRetrievalResult(
                false, System.currentTimeMillis(), null, failureMessage
        );
    }

    private TweetRetrievalResult newSuccessfulResult(String tweetMarkup)
    {
        return new TweetRetrievalResult(
                true, System.currentTimeMillis(), tweetMarkup, null
        );
    }

    public boolean matches(String url)
    {
        return SINGLE_TWEET_PATTERN.matcher(url).matches()
                || WIDGET_PATTERN.matcher(url).matches();
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        webResourceManager.requireResource("com.atlassian.confluence.extra.widgetconnector:twitter-webresources");
        return velocityRenderService.render(url, getParameters(url, params));
    }

    static class TweetRetrievalResult implements Serializable
    {
        private final boolean successful;

        private final long timestamp;

        private final String tweetMarkup;

        private final String errorMessage;

        TweetRetrievalResult()
        {
            this(false, 0, null, null);
        }

        TweetRetrievalResult(boolean successful, long timestamp, String tweetMarkup, String errorMessage)
        {
            this.successful = successful;
            this.timestamp = timestamp;
            this.tweetMarkup = tweetMarkup;
            this.errorMessage = errorMessage;
        }

        public boolean isFailedStatusTimedOut()
        {
            return System.currentTimeMillis() - timestamp > SINGLE_TWEET_RETRIEVAL_FAILURE_DELAY;
        }
    }
}
