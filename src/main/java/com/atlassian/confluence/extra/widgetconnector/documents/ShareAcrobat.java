package com.atlassian.confluence.extra.widgetconnector.documents;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;


public class ShareAcrobat implements WidgetRenderer
{
    public static final String MATCH_URL = "share.acrobat.com";
    private static final Pattern PATTERN = Pattern.compile("docid=([^&]+)");    
    public static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/embed.vm";
    private VelocityRenderService velocityRenderService;
    private static final String EMBED_URL = "https://share.acrobat.com/adc/flex/mpt.swf";
    private static final String FLASHVARS_PARAM = "flashVars";
    private static final String DEFAULT_WIDTH = "365px";
    private static final String DEFAULT_HEIGHT = "500px";


    public ShareAcrobat(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        params.put(FLASHVARS_PARAM, getFlashVars(url));

        if (!params.containsKey(VelocityRenderService.WIDTH_PARAM))
            params.put(VelocityRenderService.WIDTH_PARAM, DEFAULT_WIDTH);
        if (!params.containsKey(VelocityRenderService.HEIGHT_PARAM))
            params.put(VelocityRenderService.HEIGHT_PARAM, DEFAULT_HEIGHT);

        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(EMBED_URL, params);
    }

    public String getFlashVars(String url)
    {
        Matcher m = PATTERN.matcher(url);

        String docId = "";

        if (m.find())
        {
            docId = m.group(1);
        }

        return "docId=" + docId;
    }
}
