package com.atlassian.confluence.extra.widgetconnector.video;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.confluence.extra.widgetconnector.WidgetConnectorUtil;
import com.atlassian.confluence.extra.widgetconnector.WidgetImagePlaceholder;
import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

public class OoyalaRenderer implements WidgetRenderer, WidgetImagePlaceholder
{
    private static final String MATCH_URL = "ooyala.com";

    // WC-73 - We have to fall back to use this regex that might break easily every time Ooyala changed their URL link
    //         but at least it'll not open up XSS security risks.
    //       - Unit test name that was affected testInvalidOoyalaLinkInterpretedNull.
    private static final Pattern EMBED_CODE_PATTERN = Pattern.compile("^.*((\\?embedCode)|(\\#ec)|(\\#ooid))=([a-zA-Z0-9]+[-|_]?[a-zA-Z0-9]+).*$");
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/ooyala.vm";
    private static final String DEFAULT_WIDTH = "712px";
    private static final String DEFAULT_HEIGHT = "400px";
    private static final String PIXEL = "px";

    private final VelocityRenderService velocityRenderService;

    private final PermissionManager permissionManager;

    private final OoyalaConfigurationManager ooyalaConfigurationManager;

    public OoyalaRenderer(VelocityRenderService velocityRenderService, PermissionManager permissionManager, OoyalaConfigurationManager ooyalaConfigurationManager)
    {
        this.velocityRenderService = velocityRenderService;
        this.permissionManager = permissionManager;
        this.ooyalaConfigurationManager = ooyalaConfigurationManager;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);

        String brandingId = ooyalaConfigurationManager.getBrandingId();
        if (StringUtils.isNotBlank(brandingId))
            params.put("brandingId", brandingId);

        String embedCode = getEmbedCode(url);
        if (StringUtils.isNotBlank(embedCode))
            params.put("embedCode", embedCode);

        params.put("videoContainerId", RandomStringUtils.randomAlphabetic(8));
        params.put("currentUserAdmin", String.valueOf(permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM)));

        return velocityRenderService.render(getEmbedUrl(url, params), params);
    }

    private String getEmbedCode(String embedUrl)
    {
        // Can extract the embed code from URL formats
        // * <script height="358px" width="603px" src="http://player.ooyala.com/iframe.js#ec=55dXMxMjoBhTgtutLWhR4aG6OklYZxak&pbid=150bbbf72eee4640a0642a85f0ee5582"></script>
        // * http://player.ooyala.com/player.js?embedCode=55dXMxMjoBhTgtutLWhR4aG6OklYZxak
        // * http://support.ooyala.com/developers/otv/how-create-syndication-groups#ooid=d0OG9mNDplZplo5ecp7_nJWwSshAmv2A
        // * http://support.ooyala.com/developers/otv/how-customize-player#ooid=l4OG9mNDpdtpQ8phlgg0pH4DFgTTWSxt
        // * http://support.ooyala.com/developers/otv/player-branding-%E2%80%93-adding-custom-watermark-and-scrubber-image#ooid=BvdWYzNjoV33e2pTitH-RtxowboRbufH

        Matcher embedCodeMatcher = EMBED_CODE_PATTERN.matcher(embedUrl);
        return embedCodeMatcher.matches() ? embedCodeMatcher.group(5) : "";
    }

    public String getEmbedUrl(String url, Map<String, String> params)
    {
        String embedCode = getEmbedCode(url);

        String width = params.get(VelocityRenderService.WIDTH_PARAM);
        String height = params.get(VelocityRenderService.HEIGHT_PARAM);

        if (StringUtils.isEmpty(width))
            width = DEFAULT_WIDTH;
        else
            if (StringUtils.isNumeric(width))
                width = width.concat(PIXEL);
            else
                width = DEFAULT_WIDTH;
        
        if (StringUtils.isEmpty(height))
            height = DEFAULT_HEIGHT;
        else
            if (StringUtils.isNumeric(height))
                height = height.concat(PIXEL);
            else
                height = DEFAULT_HEIGHT;
        
        return "//player.ooyala.com/player.js?embedCode=" + embedCode + "&width=" + width + "&height=" + height;
    }

    @Override
    public ImagePlaceholder getImagePlaceholder(String url, Map<String, String> params)
    {
        return WidgetConnectorUtil.generateDefaultImagePlaceholder("player.ooyala.com");
    }
}
