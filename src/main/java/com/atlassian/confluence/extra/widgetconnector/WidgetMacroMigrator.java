package com.atlassian.confluence.extra.widgetconnector;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;

public class WidgetMacroMigrator implements MacroMigration
{
    public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
    {
        final String bodyText = macroDefinition.getBodyText();
        if (StringUtils.isNotBlank(bodyText))
        {
            macroDefinition.setParameters(
                    new HashMap<String, String>(macroDefinition.getParameters())
                    {
                        { put("url", StringUtils.strip(bodyText)); }
                    }
            );
        }

        return macroDefinition;
    }
}
