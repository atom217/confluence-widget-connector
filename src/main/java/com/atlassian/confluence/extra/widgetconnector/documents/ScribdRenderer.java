package com.atlassian.confluence.extra.widgetconnector.documents;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;


public class ScribdRenderer implements WidgetRenderer
{
    public static final String MATCH_URL = "scribd.com";
    public static final Pattern PATTERN = Pattern.compile("doc/(\\d+)/.+");
    public static final Pattern ACCESS_KEY_PATTERN = Pattern.compile("access_key=([^&]+)&amp;");
    private HttpRetrievalEmbedService httpRetrievalEmbedService;
    private static final String DEFAULT_WIDTH = "475";
    private static final String DEFAULT_HEIGHT = "355";
    public static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/scribd.vm";
    private VelocityRenderService velocityRenderService;


    public ScribdRenderer(HttpRetrievalEmbedService httpRetrievalEmbedService, VelocityRenderService velocityRenderService)
    {
        this.httpRetrievalEmbedService = httpRetrievalEmbedService;
        this.velocityRenderService = velocityRenderService;
    }

    public String getDocId(String url)
    {
        Matcher m = PATTERN.matcher(url);
        if (m.find())
        {
            return m.group(1);
        }
        return null;
    }

    public String getEmbedUrl(String url)
    {
        Matcher m = PATTERN.matcher(url);
        if (m.find())
        {
            String doc = m.group(1);
            String accessKey = httpRetrievalEmbedService.getEmbedData(url, ACCESS_KEY_PATTERN, this.getClass().getName());
            return "//documents.scribd.com/ScribdViewer.swf?document_id=" + doc + "&access_key=" + accessKey + "&version=1";
        }
        return null;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        params.put("nameHtml", getDocId(url));

        if (!params.containsKey(VelocityRenderService.WIDTH_PARAM))
            params.put(VelocityRenderService.WIDTH_PARAM, DEFAULT_WIDTH);
        if (!params.containsKey(VelocityRenderService.HEIGHT_PARAM))
            params.put(VelocityRenderService.HEIGHT_PARAM, DEFAULT_HEIGHT);

        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
