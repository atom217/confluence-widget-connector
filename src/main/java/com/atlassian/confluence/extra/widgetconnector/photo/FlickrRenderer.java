package com.atlassian.confluence.extra.widgetconnector.photo;

import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FlickrRenderer implements WidgetRenderer
{
    private static final String MATCH_URL = "flickr.com";
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/flickr.vm";

    public static final Pattern TAGS_PATTERN = Pattern.compile("/photos/tags/([^/]+)/?");
    public static final Pattern USER_PATTERN = Pattern.compile("/photos/([^/]+)/?(\\d+)?/?");
    public static final Pattern SET_PATTERN = Pattern.compile("/photos/([^/]+)/sets/(\\d+)/?");
    public static final Pattern PATTERN = Pattern.compile("id=([^&]+@[^&]+)&amp;");

    private static final String USERNAME_PLACEHOLDER = "[USER_NAME]";
    private static final String USERID_PLACEHOLDER = "[USER_ID]";
    private static final String SET_PLACEHOLDER = "[SET]";
    private static final String TAG_PLACEHOLDER = "[TAG]";
    private static final String JUMPTO_PLACEHOLDER = "[JUMPTO]";

    public static final String TAG_URL = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2Ftags%2F" + TAG_PLACEHOLDER + "%2Fshow%2F&page_show_back_url=%2Fphotos%2Ftags%2F" + TAG_PLACEHOLDER + "%2F&tags=" + TAG_PLACEHOLDER + "&jump_to=&st" + TAG_PLACEHOLDER + "_index=";
    public static final String USER_URL = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2F" + USERNAME_PLACEHOLDER + "%2Fshow%2F&page_show_back_url=%2Fphotos%2F" + USERNAME_PLACEHOLDER + "%2F&user_id=" + USERID_PLACEHOLDER + "&jump_to=";
    public static final String USER_URL_PIC = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2F" + USERNAME_PLACEHOLDER + "%2Fshow%2Fwith%2F" + JUMPTO_PLACEHOLDER + "%2F&page_show_back_url=%2Fphotos%2F" + USERNAME_PLACEHOLDER + "%2Fwith%2F" + JUMPTO_PLACEHOLDER + "%2F&user_id=" + USERID_PLACEHOLDER + "&jump_to=" + JUMPTO_PLACEHOLDER;
    public static final String SET_URL = "&offsite=true&intl_lang=en-us&page_show_url=%2Fphotos%2F" + USERNAME_PLACEHOLDER + "%2Fsets%2F" + SET_PLACEHOLDER + "%2Fshow%2F&page_show_back_url=%2Fphotos%2F" + USERNAME_PLACEHOLDER + "%2Fsets%2F" + SET_PLACEHOLDER + "%2F&set_id=" + SET_PLACEHOLDER + "&jump_to=";

    private HttpRetrievalEmbedService httpRetrievalEmbedService;
    private VelocityRenderService velocityRenderService;

    public FlickrRenderer(HttpRetrievalEmbedService httpRetrievalEmbedService, VelocityRenderService velocityRenderService)
    {
        this.httpRetrievalEmbedService = httpRetrievalEmbedService;
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        String displayUrl = null;

        Matcher m = USER_PATTERN.matcher(url);

        if (m.find() && !url.contains("photos/tags/"))
            displayUrl = renderUser(m, url);

        m = SET_PATTERN.matcher(url);
        if (m.find() && !url.contains("photos/tags/"))
            displayUrl = renderSet(m);

        m = TAGS_PATTERN.matcher(url);
        if (m.find())
            displayUrl = renderTag(m);

        return displayUrl;
    }

    private String renderSet(Matcher m)
    {
        return SET_URL.replace(USERNAME_PLACEHOLDER, m.group(1)).replace(SET_PLACEHOLDER, m.group(2));
    }

    private String renderUser(Matcher m, String url)
    {
        String username = m.group(1);
        String jumpTo = m.group(2);
        String userId = httpRetrievalEmbedService.getEmbedData("https://www.flickr.com/photos/" + username, PATTERN, this.getClass().getName());
        if (userId == null)
            return null;
        if (StringUtils.isNotEmpty(jumpTo))
            return USER_URL_PIC.replace(USERNAME_PLACEHOLDER, username).replace(USERID_PLACEHOLDER, userId).replace(JUMPTO_PLACEHOLDER, jumpTo);
        else
            return USER_URL.replace(USERNAME_PLACEHOLDER, username).replace(USERID_PLACEHOLDER, userId);
    }


    private String renderTag(Matcher m)
    {
        return TAG_URL.replace(TAG_PLACEHOLDER, m.group(1));
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
