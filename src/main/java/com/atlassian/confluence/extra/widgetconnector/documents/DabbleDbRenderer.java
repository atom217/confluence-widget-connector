package com.atlassian.confluence.extra.widgetconnector.documents;

import com.atlassian.confluence.extra.widgetconnector.WidgetConnectorUtil;
import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DabbleDbRenderer implements WidgetRenderer
{
    private static final Logger log = LoggerFactory.getLogger(DabbleDbRenderer.class);

    public static final String MATCH_URL = "dabbledb.com";
    public static final Pattern PATTERN = Pattern.compile("dabbledb\\.com/page/[^/]+/[^/]+");
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/iframe.vm";
    private static final String DEFAULT_WIDTH = "100%";
    private static final String DEFAULT_HEIGHT = "500px";
    private VelocityRenderService velocityRenderService;

    public DabbleDbRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        Matcher m = PATTERN.matcher(url);

        if (m.find())
        {
            return url + "?embed=true";
        }
        return null;
    }


    public boolean matches(String url)
    {
        return WidgetConnectorUtil.isURLMatch(url, MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        if (!params.containsKey(VelocityRenderService.WIDTH_PARAM))
            params.put(VelocityRenderService.WIDTH_PARAM, DEFAULT_WIDTH);
        if (!params.containsKey(VelocityRenderService.HEIGHT_PARAM))
            params.put(VelocityRenderService.HEIGHT_PARAM, DEFAULT_HEIGHT);

        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}