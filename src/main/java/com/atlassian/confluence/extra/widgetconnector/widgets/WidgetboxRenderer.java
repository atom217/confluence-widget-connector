package com.atlassian.confluence.extra.widgetconnector.widgets;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;

public class WidgetboxRenderer implements WidgetRenderer
{
    private static final Pattern PATTERN = Pattern.compile("confluence/([^?]+)");
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/widgetbox.vm";
    private static final String EMBED_URL = "//cdn.widgetserver.com/syndication/subscriber/InsertWidget.js";
    private static final String MATCH_URL = "widgetbox.com";
    private static final String IDCODE_PARAM = "idcode";

    private VelocityRenderService velocityRenderService;

    public WidgetboxRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        params.put(IDCODE_PARAM, getIdCode(url));
        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(EMBED_URL, params);
    }

    public String getIdCode(String url)
    {
        Matcher m = PATTERN.matcher(url);
        String idCode = "";

        if (m.find())
        {
            idCode = m.group(1);
        }

        return idCode;
    }
}
