package com.atlassian.confluence.extra.widgetconnector.widgets;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;

public class GoogleGadgetsRenderer implements WidgetRenderer
{
    private static final Pattern PATTERN = Pattern.compile("url=([^&]+)&?");
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/googlegadgets.vm";
    private static final String DEFAULT_WIDTH = "410px";
    private static final String DEFAULT_HEIGHT = "342px";
    private VelocityRenderService velocityRenderService;
    private static final String MATCH_URL = "google.com/ig";

    public GoogleGadgetsRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        Matcher m = PATTERN.matcher(url);
        String gadgetUrl;
        if (m.find())
        {
            gadgetUrl =  m.group(1);
            if (!gadgetUrl.startsWith("http"))
                gadgetUrl = "//".concat(gadgetUrl);
            return gadgetUrl;
        }
        return null;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        if (!params.containsKey(VelocityRenderService.WIDTH_PARAM))
            params.put(VelocityRenderService.WIDTH_PARAM, DEFAULT_WIDTH);
        if (!params.containsKey(VelocityRenderService.HEIGHT_PARAM))
            params.put(VelocityRenderService.HEIGHT_PARAM, DEFAULT_HEIGHT);

        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
