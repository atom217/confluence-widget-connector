package com.atlassian.confluence.extra.widgetconnector.video;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;

public class GoogleVideoRenderer implements WidgetRenderer
{
    private static final String MATCH_URL = "video.google.com";
    private static final String PATTERN = "docid=([^&]+)&?";
    private VelocityRenderService velocityRenderService;

    public GoogleVideoRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        Pattern p = Pattern.compile(PATTERN);
        Matcher m = p.matcher(url);

        String videoId = "";

        if (m.find())
        {
            videoId = m.group(1);
        }
        return "//video.google.com/googleplayer.swf?docid="+ videoId +"&hl=en&fs=true";
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
