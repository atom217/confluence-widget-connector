package com.atlassian.confluence.extra.widgetconnector.services;

import com.atlassian.confluence.macro.ImagePlaceholder;
import java.util.Map;

public interface PlaceholderService
{
    public ImagePlaceholder generatePlaceholder(String url, Map<String, String> params);
}
