package com.atlassian.confluence.extra.widgetconnector.video;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ViddlerRenderer implements WidgetRenderer
{
    private static final String MATCH_URL = "viddler.com";

    public static final Pattern PATTERN = Pattern.compile("www.viddler.com/player/([a-z0-9]+)");

    private static final Pattern VIDEO_PATTERN = Pattern.compile("www.viddler.com/v/((?:[a-z0-9]*))");

    private static final Pattern PRIVATE_VIDEO_PATTERN = Pattern.compile("(https?://)?www.viddler.com/v/.*?(secret=(.+?))(\\&.+)*$");

    private final VelocityRenderService velocityRenderService;
    private final HttpRetrievalEmbedService httpRetrievalEmbedService;

    public ViddlerRenderer(HttpRetrievalEmbedService httpRetrievalEmbedService, VelocityRenderService velocityRenderService)
    {
        this.httpRetrievalEmbedService = httpRetrievalEmbedService;
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        String embedParameter = httpRetrievalEmbedService.getEmbedData(url, PATTERN, this.getClass().getName());
        return "//www.viddler.com/player/" + embedParameter;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        url = httpRetrievalEmbedService.getNewLocation(url);
        Matcher m = VIDEO_PATTERN.matcher(url);

        String videoId = "";

        if (m.find())
        {
            videoId = m.group(1);
        }

        params.put("flashVars", "key=" + videoId);

        // WC-43 - Support for private video links
        m = PRIVATE_VIDEO_PATTERN.matcher(url);
        if (m.matches())
        {
            params.put("flashVars", "key=" + videoId + "&openURL=" + m.group(3));
        }

        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}