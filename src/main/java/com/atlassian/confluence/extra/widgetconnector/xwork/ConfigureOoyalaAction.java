package com.atlassian.confluence.extra.widgetconnector.xwork;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.widgetconnector.video.OoyalaConfigurationManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.apache.commons.lang.StringUtils;

@WebSudoRequired
public class ConfigureOoyalaAction extends ConfluenceActionSupport
{
    private String brandingId;

    private String returnUrl;

    private boolean success;

    private OoyalaConfigurationManager ooyalaConfigurationManager;

    @SuppressWarnings("unused")
    public void setOoyalaConfigurationManager(OoyalaConfigurationManager ooyalaConfigurationManager)
    {
        this.ooyalaConfigurationManager = ooyalaConfigurationManager;
    }

    public String getBrandingId()
    {
        return brandingId;
    }

    public void setBrandingId(String brandingId)
    {
        this.brandingId = brandingId;
    }

    @SuppressWarnings("unused")
    public String getReturnUrl()
    {
        return returnUrl;
    }

    @SuppressWarnings("unused")
    public void setReturnUrl(String returnUrl)
    {
        this.returnUrl = returnUrl;
    }

    @SuppressWarnings("unused")
    public boolean isSuccess()
    {
        return success;
    }

    @SuppressWarnings("unused")
    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    @Override
    public String doDefault() throws Exception
    {
        setBrandingId(ooyalaConfigurationManager.getBrandingId());
        return super.doDefault();
    }

    @Override
    public String execute() throws Exception
    {
        ooyalaConfigurationManager.setBrandingId(getBrandingId());
        if (StringUtils.isBlank(getReturnUrl()))
            setReturnUrl("/admin/widgetconnector/configureooyala.action?success=true");

        return SUCCESS;
    }
}
