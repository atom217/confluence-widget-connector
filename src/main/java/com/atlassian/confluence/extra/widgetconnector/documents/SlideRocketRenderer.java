package com.atlassian.confluence.extra.widgetconnector.documents;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SlideRocketRenderer implements WidgetRenderer
{
    private static final Pattern PATTERN = Pattern.compile("id=([^&]+)&?");
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/iframe.vm";
    private VelocityRenderService velocityRenderService;

    public SlideRocketRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        Matcher m = PATTERN.matcher(url);
        String videoId = "";

        if (m.find())
        {
            videoId = m.group(1);
        }
        return "//app.sliderocket.com/app/FullPlayer.aspx?id=" + videoId;
    }

    public boolean matches(String url)
    {
        return url.contains("sliderocket.com");
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
