package com.atlassian.confluence.extra.widgetconnector.video;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.http.HttpRequest;
import com.atlassian.confluence.util.http.HttpResponse;
import com.atlassian.confluence.util.http.HttpRetrievalService;

public class EpisodicRenderer implements WidgetRenderer
{
    private static final String MATCH_URL = "episodic.com";
    private static final String PATTERN = "shows/(.+)/episodes/(.+)/?";
    private VelocityRenderService velocityRenderService;
    private HttpRetrievalService httpRetrievalService;

    public EpisodicRenderer(VelocityRenderService velocityRenderService, HttpRetrievalService httpRetrievalService)
    {
        this.velocityRenderService = velocityRenderService;
        this.httpRetrievalService = httpRetrievalService;
    }
    
	public String getEmbedUrl(String url)
    {
        Pattern p = Pattern.compile(PATTERN);
        Matcher m = p.matcher(url);

        String showId = "";
        String episodeId = "";
        String videoId = "";

        if (m.find())
        {
            showId = m.group(1);
            episodeId = m.group(2);
        }
        
        try {
			if (isNewVideoFormat(showId, episodeId)) 
			{
				videoId = showId + "%2F" + episodeId.substring(episodeId.length()-1, episodeId.length())+ "%2F" + episodeId;
			}
			else
			{
				videoId = showId + "%2F" + episodeId;
			}
		} catch (IOException e) {
			 // return null here to display error template
			 return null;
		}
        
        return "//cdn.episodic.com/player/EpisodicPlayer.swf?config=http%3A%2F%2Fcdn.episodic.com%2Fshows%2F" + videoId + "%2fconfig.xml";
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
       return velocityRenderService.render(getEmbedUrl(url), params);
    }
    
    /**
     * determine whether the video is using new format by looking at the content returned by the URL to its configuration XML file
     * @param showId
     * @param episodeId
     * @throws IOException 
     */
    private boolean isNewVideoFormat(String showId, String episodeId) throws IOException
    {
    	StringBuilder s = new StringBuilder();
        s.append("https://cdn.episodic.com/shows/");
    	s.append(GeneralUtil.urlEncode(showId));
    	s.append("/");
    	s.append(GeneralUtil.urlEncode(episodeId));
    	s.append("/config.xml");
    	HttpRequest req = httpRetrievalService.getDefaultRequestFor(s.toString());
    	HttpResponse resp = httpRetrievalService.get(req);
		return resp.isNotFound() || resp.isNotPermitted() || resp.isFailed();
    }

}
