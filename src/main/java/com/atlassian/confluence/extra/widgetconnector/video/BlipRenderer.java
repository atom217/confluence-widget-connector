package com.atlassian.confluence.extra.widgetconnector.video;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.regex.Pattern;


public class BlipRenderer implements WidgetRenderer
{
    private HttpRetrievalEmbedService httpRetrievalEmbedService;

    public static final String MATCH_URL = "blip.tv";
    public static final Pattern PATTERN = Pattern.compile("http://blip.tv/play/([^\\\\]+)");
    private VelocityRenderService velocityRenderService;


    public BlipRenderer(HttpRetrievalEmbedService httpRetrievalEmbedService, VelocityRenderService velocityRenderService)
    {
        this.httpRetrievalEmbedService = httpRetrievalEmbedService;
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        String ajaxUrl = url+"?skin=json";
        String embedParameter = httpRetrievalEmbedService.getEmbedData(ajaxUrl, PATTERN, this.getClass().getName());
        if (StringUtils.isNotEmpty(embedParameter))
            return "//blip.tv/play/" + embedParameter;

        return null;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}

