package com.atlassian.confluence.extra.widgetconnector.video;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.confluence.extra.widgetconnector.WidgetImagePlaceholder;
import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.DefaultPlaceholderService;
import com.atlassian.confluence.extra.widgetconnector.services.PlaceholderService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.macro.ImagePlaceholder;

import org.apache.commons.lang.StringUtils;


public class YoutubeRenderer implements WidgetRenderer, WidgetImagePlaceholder
{
    private static final Pattern YOUTUBE_URL_PATTERN = Pattern.compile("https?://(.+\\.)?youtube.com.*(\\?v=([^&]+)).*$");

    private VelocityRenderService velocityRenderService;
    private final PlaceholderService placeholderService;
    private final String DEFAULT_YOUTUBE_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/youtube.vm";
    private final String DEFAULT_WIDTH = "400px";
    private final String DEFAULT_HEIGHT = "300px";
    private final String PIXEL = "px";

    // Constants ordered by actual quality (top - highest > bottom - lowest)
    private final String MAX_RES_THUMBNAIL      = "maxresdefault.jpg";
    private final String HIGH_QUALITY_THUMBNAIL = "hqdefault.jpg";
    private final String MED_QUALITY_THUMBNAIL  = "mqdefault.jpg";
    private final String LOW_QUALITY_THUMBNAIL  = "default.jpg";

    public YoutubeRenderer(VelocityRenderService velocityRenderService, PlaceholderService placeholderService)
    {
        this.velocityRenderService = velocityRenderService;
        this.placeholderService = placeholderService;
    }

    public String getEmbedUrl(String url)
    {
        Matcher youtubeUrlMatcher = YOUTUBE_URL_PATTERN.matcher(verifyEmbeddedPlayerString(url));
        return youtubeUrlMatcher.matches()
                ? String.format("//www.youtube.com/embed/%s?wmode=opaque", youtubeUrlMatcher.group(3))
                : null;
    }

    public boolean matches(String url)
    {
        return YOUTUBE_URL_PATTERN.matcher(verifyEmbeddedPlayerString(url)).matches();
    }

    private String verifyEmbeddedPlayerString(String url)
    {
        return !url.contains("feature=player_embedded&")? url : url.replace("feature=player_embedded&", StringUtils.EMPTY);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        return velocityRenderService.render(getEmbedUrl(url), setDefaultParam(params));
    }
    
    private Map<String, String> setDefaultParam(Map<String, String> params)
    {
    	String width = params.get(VelocityRenderService.WIDTH_PARAM);
        String height = params.get(VelocityRenderService.HEIGHT_PARAM);
    	if (!params.containsKey((VelocityRenderService.TEMPLATE_PARAM)))  {
    		params.put(VelocityRenderService.TEMPLATE_PARAM, DEFAULT_YOUTUBE_TEMPLATE);
    	}
    	if (StringUtils.isEmpty(width)) {
    		params.put(VelocityRenderService.WIDTH_PARAM, DEFAULT_WIDTH);
    	}
    	else {
    		if (StringUtils.isNumeric(width)) {
    			params.put(VelocityRenderService.WIDTH_PARAM, width.concat(PIXEL));
    		}
    	}
    	if (StringUtils.isEmpty(height)) {
    		params.put(VelocityRenderService.HEIGHT_PARAM, DEFAULT_HEIGHT);
    	}
    	else {
    		if (StringUtils.isNumeric(height)) {
    			params.put(VelocityRenderService.HEIGHT_PARAM, height.concat(PIXEL));
    		}
    	}
    	return params;
    }

    private Map<String, String> populateImagePlaceholderParam(Map<String, String> params)
    {
        params = setDefaultParam(params);

        params.put(DefaultPlaceholderService.PARAM_OVERLAY_URL, "youtube");

        return params;
    }

    private String getThumbnailUrl(String url)
    {
        Matcher youtubeUrlMatcher = YOUTUBE_URL_PATTERN.matcher(url);
        return youtubeUrlMatcher.matches()
                ? String.format("http://img.youtube.com/vi/%s/" + MED_QUALITY_THUMBNAIL, youtubeUrlMatcher.group(3))
                : null;
    }

    public ImagePlaceholder getImagePlaceholder(String url, Map<String, String> params)
    {
        return placeholderService.generatePlaceholder(getThumbnailUrl(url), populateImagePlaceholderParam(params));
    }
}
