package com.atlassian.confluence.extra.widgetconnector.documents;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.util.GeneralUtil;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class GoogleDocsRenderer implements WidgetRenderer
{
    private static final Pattern DOCUMENT_PATTERN = Pattern.compile("(document)/d/([^/]+)&?");
    private static final Pattern PRESENTATION_PATTERN = Pattern.compile("(presentation)/d/([^/]+)&?");

    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/googledocs.vm";
    private static final String DEFAULT_WIDTH = "768";
    private static final String DEFAULT_HEIGHT = "342";

    public static final String PRESENTATION_TYPE = "presentation";
    public static final String DOCUMENT_TYPE = "document";

    public static final String PRESENTATION_EMBED_URL = "/embed";
    public static final String DOCUMENT_EMBED_URL = "/pub?embedded=true";
    private static final String MATCH_URL = "docs.google.com";
    private static final String EMBED_URL = "//docs.google.com/";

    private VelocityRenderService velocityRenderService;

    public static final Collection<Pattern> MATCHER_PATTERNS;
    static
    {
        List<Pattern> matcherPatterns = new ArrayList<Pattern>();
        matcherPatterns.add(DOCUMENT_PATTERN);
        matcherPatterns.add(PRESENTATION_PATTERN);
        MATCHER_PATTERNS = Collections.unmodifiableList(matcherPatterns);
    }

    public GoogleDocsRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        for (Pattern eachPattern : MATCHER_PATTERNS)
        {
            Matcher matcher = eachPattern.matcher(url);
            if (matcher.find())
            {
                String docType = matcher.group(1);
                String docId = matcher.group(2);

                String embedUrl = EMBED_URL + docType + "/d/" + docId;
                if (PRESENTATION_TYPE.equals(docType))
                {
                    embedUrl += PRESENTATION_EMBED_URL;
                }
                else if (DOCUMENT_TYPE.equals(docType))
                {
                    embedUrl += DOCUMENT_EMBED_URL;
                }
                return embedUrl;
            }
        }
        return null;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        if (!params.containsKey(VelocityRenderService.WIDTH_PARAM))
            params.put(VelocityRenderService.WIDTH_PARAM, DEFAULT_WIDTH);
        if (!params.containsKey(VelocityRenderService.HEIGHT_PARAM))
            params.put(VelocityRenderService.HEIGHT_PARAM, DEFAULT_HEIGHT);

        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}