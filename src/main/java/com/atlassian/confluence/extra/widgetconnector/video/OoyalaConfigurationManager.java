package com.atlassian.confluence.extra.widgetconnector.video;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import org.apache.commons.lang3.StringUtils;

public class OoyalaConfigurationManager
{
    public static final String WC_CFG_OOYALA_BRANDING_ID = "wc_ooyala_brandingid";

    private final BandanaManager bandanaManager;

    public OoyalaConfigurationManager(BandanaManager bandanaManager)
    {
        this.bandanaManager = bandanaManager;
    }

    public void setBrandingId(String brandingId)
    {
        if (StringUtils.isBlank(brandingId))
        {
            bandanaManager.removeValue(
                    ConfluenceBandanaContext.GLOBAL_CONTEXT,
                    WC_CFG_OOYALA_BRANDING_ID
            );
        }
        else
        {
            bandanaManager.setValue(
                    ConfluenceBandanaContext.GLOBAL_CONTEXT,
                    WC_CFG_OOYALA_BRANDING_ID,
                    brandingId
            );
        }
    }


    public String getBrandingId()
    {
//        return "58361ea0eb814c24a42c53dafb144e08"; // Test branding ID from Ooyala
        return (String) bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                WC_CFG_OOYALA_BRANDING_ID
        );
    }
}
