package com.atlassian.confluence.extra.widgetconnector;

import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DefaultRenderManager implements RenderManager
{
    private List<WidgetRenderer> renderSupporter;
    public static final String ERROR_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/error.vm";
    public static final Pattern BASEURL_PATTERN = Pattern.compile("^https?://([^/]++)(?:/.*)?");

    public DefaultRenderManager(List<WidgetRenderer> renderSupporter)
    {
        this.renderSupporter = renderSupporter;
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        for (WidgetRenderer widgetRenderer : renderSupporter)
        {
            if (widgetRenderer.matches(url))
            {
                String embedHtml = widgetRenderer.getEmbeddedHtml(url, params);
                if (StringUtils.isNotEmpty(embedHtml))
                    return embedHtml;
            }
        }

        // If the renderer was unable to return a renderered template we will display the error template.
        Map<String, Object> contextMap = getDefaultVelocityContext();
        contextMap.put("urlHtml", GeneralUtil.htmlEncode(url));
        contextMap.put("baseUrlHtml", GeneralUtil.htmlEncode(getBaseUrl(url)));
        return getRenderedTemplate(contextMap);
    }

    public ImagePlaceholder getImagePlaceholder(String url, Map<String, String> params)
    {
        for (WidgetRenderer widgetRenderer : renderSupporter)
        {
            if (widgetRenderer.matches(url))
            {
                if(widgetRenderer instanceof WidgetImagePlaceholder)
                {
                    ImagePlaceholder placeholder = ((WidgetImagePlaceholder) widgetRenderer).getImagePlaceholder(url, params);
                    if (placeholder != null)
                    {
                        return placeholder;
                    }
                }
                else
                {
                    break;
                }

            }
        }

        return WidgetConnectorUtil.generateDefaultImagePlaceholder(getBaseUrl(url));
    }

    public String getBaseUrl(String url)
    {
        Matcher m = BASEURL_PATTERN.matcher(url);

        if (m.find())
        {
            return m.group(1);
        }
        return url;
    }
    
    protected Map<String, Object> getDefaultVelocityContext()
    {
        return MacroUtils.defaultVelocityContext();
    }

    protected String getRenderedTemplate(Map<String, Object> contextMap)
    {
        return VelocityUtils.getRenderedTemplate(ERROR_TEMPLATE, contextMap);
    }
}
