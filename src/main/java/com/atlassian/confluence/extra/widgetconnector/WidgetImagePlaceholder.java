package com.atlassian.confluence.extra.widgetconnector;

import com.atlassian.confluence.macro.ImagePlaceholder;
import org.springframework.aop.TargetClassAware;

import java.util.Map;

public interface WidgetImagePlaceholder {
    /**
     * Returns an image placeholder for used in the confluence WYSIWYG editor
     * @param url The URL given by the user
     * @param params Additional parameters for internal use. For example, width, height, etc
     * @return
     */
    ImagePlaceholder getImagePlaceholder(String url, Map<String, String> params);
}
