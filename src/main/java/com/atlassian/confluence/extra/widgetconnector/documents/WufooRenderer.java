package com.atlassian.confluence.extra.widgetconnector.documents;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class WufooRenderer implements WidgetRenderer
{
    public static final String MATCH_URL = "wufoo.com";
    public static final Pattern PATTERN = Pattern.compile("http://([^.]+)\\.wufoo\\.com/([^/]+)/([^/]+)");
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/iframe.vm";
    private static final String DEFAULT_WIDTH = "100%";
    private static final String DEFAULT_HEIGHT = "500px";
    private VelocityRenderService velocityRenderService;


    public WufooRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        Matcher m = PATTERN.matcher(url);

        String userId = "";
        String type = "";
        String id = "";

        if (m.find())
        {
            userId = m.group(1);
            type = m.group(2);
            id = m.group(3);

            if ("reports".equals(type)) {
                // There's no iframe embed functionality available from Wufoo for reports. Let's just embed the report
                // page as is.
                return "//" + userId + ".wufoo.com/reports/" + id + "/";
            }

            return "//" + userId + ".wufoo.com/embed/" + id + "/";
        }
        return null;
    }


    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        if (!params.containsKey(VelocityRenderService.WIDTH_PARAM))
            params.put(VelocityRenderService.WIDTH_PARAM, DEFAULT_WIDTH);
        if (!params.containsKey(VelocityRenderService.HEIGHT_PARAM))
            params.put(VelocityRenderService.HEIGHT_PARAM, DEFAULT_HEIGHT);

        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
