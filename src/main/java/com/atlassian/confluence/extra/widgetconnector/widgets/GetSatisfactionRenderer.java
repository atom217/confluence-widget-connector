package com.atlassian.confluence.extra.widgetconnector.widgets;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;

public class GetSatisfactionRenderer implements WidgetRenderer
{
    private static final Pattern PATTERN = Pattern.compile("getsatisfaction.com/([^/]+)");
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/getsatisfaction.vm";
    private VelocityRenderService velocityRenderService;
    private static final String MATCH_URL = "getsatisfaction.com";
    private static final String COMPANY_PARAM = "company";

    public GetSatisfactionRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getCompany(String url)
    {
        Matcher m = PATTERN.matcher(url);
        String company;
        if (m.find())
        {
            company =  m.group(1);
            return company;
        }
        return null;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        params.put(COMPANY_PARAM, getCompany(url));

        return velocityRenderService.render(getCompany(url), params);
    }
}
