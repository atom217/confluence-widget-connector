package com.atlassian.confluence.extra.widgetconnector.photo;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;

import java.util.Map;
import java.util.regex.Pattern;


public class SkitchRenderer implements WidgetRenderer
{
    private static final String MATCH_URL = "skitch.com";
    private static final Pattern PATTERN = Pattern.compile("(<img src=\"//img.skitch.com/[^.]+\\.jpg\" alt=\"[^\"]+\"/>)");

    private HttpRetrievalEmbedService httpRetrievalEmbedService;

    public SkitchRenderer(HttpRetrievalEmbedService httpRetrievalEmbedService)
    {
        this.httpRetrievalEmbedService = httpRetrievalEmbedService;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        return httpRetrievalEmbedService.getEmbedData(url, PATTERN, this.getClass().getName());
    }
}
