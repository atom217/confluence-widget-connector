package com.atlassian.confluence.extra.widgetconnector.services;

import com.atlassian.confluence.extra.widgetconnector.WidgetConnectorUtil;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.pages.thumbnail.Dimensions;
import com.atlassian.confluence.util.http.HttpRetrievalService;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Map;

public class DefaultPlaceholderService implements PlaceholderService
{
    public static final String PLACEHOLDER_SERVLET = "/plugins/servlet/widgetconnector/placeholder";
    public static final String PARAM_THUMB_URL     = "thumb";
    public static final String PARAM_OVERLAY_URL   = "overlay";
    public static final String PARAM_WIDTH         = VelocityRenderService.WIDTH_PARAM;
    public static final String PARAM_HEIGHT        = VelocityRenderService.HEIGHT_PARAM;

    private final HttpRetrievalService httpRetrievalService;

    private static final int MAX_WIDTH  = 800;
    private static final int MAX_HEIGHT = 600;

    private static final Logger log = Logger.getLogger(DefaultPlaceholderService.class);

    public DefaultPlaceholderService(HttpRetrievalService httpRetrievalService)
    {
        this.httpRetrievalService = httpRetrievalService;
    }

    public ImagePlaceholder generatePlaceholder(String thumbUrl, Map<String, String> params)
    {
        if(params == null)
        {
            log.warn("Invalid parameters map. Reverting to default mode.");
            return WidgetConnectorUtil.generateDefaultImagePlaceholder(thumbUrl);
        }

        String widthStr   = params.get(PARAM_WIDTH);
        String heightStr  = params.get(PARAM_HEIGHT);
        String overlayUrl = params.get(PARAM_OVERLAY_URL);

        if(thumbUrl == null || widthStr == null || heightStr == null || overlayUrl == null ||
           thumbUrl.isEmpty() || widthStr.isEmpty() || heightStr.isEmpty() || overlayUrl.isEmpty())
        {
            log.warn("Invalid url or width or height or placeholder parameters. Reverting to default mode.");
            return WidgetConnectorUtil.generateDefaultImagePlaceholder(thumbUrl);
        }

        try
        {
            httpRetrievalService.get(thumbUrl);
        }
        catch (IOException e)
        {
            //Do nothing in this case, because only need to check whether the url is a valid http url
            //IOException can be caused by, for example, no internet, or server is down
            //Either of which is fine and will be handled by the generator servlet properly
        }
        catch (Exception e)
        {
            //This is the important exception to catch as to avoid malicious party from accessing
            //local files through file:// etc
            log.warn("Thumbnail url is not a valid http url. Reverting to default mode.");
            return WidgetConnectorUtil.generateDefaultImagePlaceholder(null);
        }

        int width, height;
        try
        {
            width  = Integer.parseInt(widthStr.replace("px", ""));
            height = Integer.parseInt(heightStr.replace("px", ""));
        }
        catch (NumberFormatException e)
        {
            log.warn("Invalid width or height values. Reverting to default mode.");
            return WidgetConnectorUtil.generateDefaultImagePlaceholder(thumbUrl);
        }

        Dimensions dimension = restrictDimensions(width, height);
        String command = createThumbnailRedirectCommand(thumbUrl, overlayUrl, dimension);
        return new DefaultImagePlaceholder(command, dimension, true);
    }

    private String createThumbnailRedirectCommand(String thumbUrl, String overlayUrl, Dimensions d)
    {
        return String.format("%s?%s=%s&%s=%s&%s=%d&%s=%d", PLACEHOLDER_SERVLET,
                PARAM_THUMB_URL, thumbUrl, PARAM_OVERLAY_URL, overlayUrl,
                PARAM_WIDTH, d.getWidth(), PARAM_HEIGHT, d.getHeight());
    }

    private Dimensions restrictDimensions(int width, int height) {
        return (new Dimensions(Math.min(MAX_WIDTH, width), Math.min(MAX_HEIGHT, height)));
    }
}
