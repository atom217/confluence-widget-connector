package com.atlassian.confluence.extra.widgetconnector.services;

import java.util.regex.Pattern;


public interface HttpRetrievalEmbedService
{
    public String getEmbedData(String url, Pattern pattern, String cacheName);

    public String getNewLocation(String oldUrl);
}
