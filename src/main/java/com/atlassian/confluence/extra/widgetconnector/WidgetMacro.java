package com.atlassian.confluence.extra.widgetconnector;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.EditorImagePlaceholder;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.thumbnail.Dimensions;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.macro.ResourceAware;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class WidgetMacro extends BaseMacro implements Macro, EditorImagePlaceholder
{
    private final RenderManager renderManager;

    private final LocaleManager localeManager;

    private final I18NBeanFactory i18NBeanFactory;

    // Required Parameter
    public static final String URL = "url";

    public WidgetMacro(RenderManager renderManager, LocaleManager localeManager, I18NBeanFactory i18NBeanFactory)
    {
        this.renderManager = renderManager;
        this.localeManager = localeManager;
        this.i18NBeanFactory = i18NBeanFactory;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    public boolean hasBody()
    {
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        return execute(parameters, body, new DefaultConversionContext(renderContext));
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext)
    {
        String url = RenderUtils.getParameter(parameters, URL, 0);
        if (StringUtils.isEmpty(url))
            url = StringUtils.strip(body); // Maintained for the old execute method

        if (StringUtils.isEmpty(url))
            return RenderUtils.blockError(getText("macro.error.urlnotspecified"), "");

       return renderManager.getEmbeddedHtml(url, parameters);
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    public String getText(String i18nKey)
    {
        return getI18NBean().getText(i18nKey);
    }

    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }
    
    public ImagePlaceholder getImagePlaceholder(Map<String, String> parameters, ConversionContext conversionContext)
    {
        String url = RenderUtils.getParameter(parameters, URL, 0);
        return renderManager.getImagePlaceholder(url, parameters);
    }
}
