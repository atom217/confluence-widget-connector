package com.atlassian.confluence.extra.widgetconnector.services;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.spring.container.ContainerManager;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.Graphics2D;
import java.awt.Font;
import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.apache.log4j.Logger;

public class ImageGeneratorServlet extends HttpServlet
{
    private static final String RESOURCE_FOLDER      = "com/atlassian/confluence/extra/widgetconnector/";
    private static final String WIDGET_LOGO_RESOURCE = RESOURCE_FOLDER + "templates/widget.png";
    private static final float OVERLAY_MAX_WIDTH     = 0.28f;
    private static final float OVERLAY_PADDING       = 0.03f;

    private static final int DEFAULT_PLACEHOLDER_MAX_NAME_LENGTH = 25;
    private static final int DEFAULT_WIDTH = 300;
    private static final int DEFAULT_HEIGHT = 225;

    private static final Logger log = Logger.getLogger(ImageGeneratorServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String thumbUrl        = req.getParameter(DefaultPlaceholderService.PARAM_THUMB_URL);
        String overlayUrl      = req.getParameter(DefaultPlaceholderService.PARAM_OVERLAY_URL);

        String stringWidth     = req.getParameter(DefaultPlaceholderService.PARAM_WIDTH);
        String stringHeight    = req.getParameter(DefaultPlaceholderService.PARAM_HEIGHT);

        int width = parseInt(stringWidth, DEFAULT_WIDTH);
        int height = parseInt(stringHeight, DEFAULT_HEIGHT);

        BufferedImage placeholder = null;
        if(overlayUrl != null && thumbUrl != null)
        {
            placeholder = createThumbnailPlaceholder(thumbUrl, overlayUrl, width, height);
        }

        if(placeholder == null)
        {
            placeholder = createDefaultPlaceholder(thumbUrl, width, height);
        }
        resp.setContentType("image/png");
        ImageIO.write(placeholder, "png", resp.getOutputStream());
    }

    private int parseInt(String intString, int defaultValue)
    {
        if (intString == null || intString.length() <= 0)
            return defaultValue;

        try
        {
            return Integer.parseInt(intString);
        }
        catch (NumberFormatException formatException)
        {
            return defaultValue;
        }
    }

    private BufferedImage createDefaultPlaceholder(String baseUrl, int width, int height) throws IOException
    {
        ClassLoader loader       = ImageGeneratorServlet.class.getClassLoader();
        InputStream widgetLogoIn = loader.getResourceAsStream(WIDGET_LOGO_RESOURCE);
        BufferedImage widgetLogo = ImageIO.read(widgetLogoIn);

        int thumbWidth  = width;
        int padding     = (int) (thumbWidth * OVERLAY_PADDING);
        int thumbHeight = widgetLogo.getHeight() + (2 * padding);

        BufferedImage placeholder = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = getHighQualityGraphicsRenderMode(placeholder);

        Font font = new Font("Arial", Font.PLAIN, 12);
        g.setFont(font);
        g.setColor(Color.DARK_GRAY);

        //Capping the url, so it looks nicer and doesn't overlap with the widget logo
        if(baseUrl.length() > DEFAULT_PLACEHOLDER_MAX_NAME_LENGTH)
        {
            baseUrl  = baseUrl.substring(0, DEFAULT_PLACEHOLDER_MAX_NAME_LENGTH);
            baseUrl += "...";
        }

        int txtWidth  = g.getFontMetrics().stringWidth(baseUrl);

        int posX = padding;
        int posY = padding;
        g.drawImage(widgetLogo, posX, posY, widgetLogo.getWidth(), widgetLogo.getHeight(), null);

        int widgetFullWidth = widgetLogo.getWidth() + (2*padding);
        int offsetX         = (int)((thumbWidth - widgetFullWidth)/2.0) + widgetFullWidth;
        int txtPosX         = offsetX - (int)(txtWidth/2.0);
        int txtPosY         = (int)(thumbHeight/2.0);
        g.drawString(baseUrl, txtPosX, txtPosY);

        g.dispose();
        return placeholder;
    }

    private BufferedImage createThumbnailPlaceholder(String thumbUrl, String overlayUrl, int width, int height)
    {
        ClassLoader loader    = ImageGeneratorServlet.class.getClassLoader();
        InputStream overlayIn = loader.getResourceAsStream(RESOURCE_FOLDER + "logos/" + overlayUrl + ".png");
        InputStream shadeIn   = loader.getResourceAsStream(RESOURCE_FOLDER + "logos/shade.png");

        if (shadeIn == null || overlayIn == null)
        {
            log.warn("Cannot load resource for shade and/or overlay. Reverting to default mode.");
            return null;
        }

        BufferedImage thumbnail = null;
        BufferedImage overlay   = null;
        BufferedImage shade     = null;
        boolean genericOverlay  = false;

        try
        {
            //The thumbUrl is generated internally by each respective renderers to be read here
            thumbnail = ImageIO.read(new URL(thumbUrl));
        }
        catch (IOException e)
        {
            // If for some reason the thumbnail image couldn't be retrieved
            // For example: No internet connection
            //              Selected thumbnail resolution is not available for the video
            //                (Such as attempting to get 720p thumbnail from 180p video)
            // Then fall back to generic logo overlay
            genericOverlay = true;
            log.warn("Reverting to generic overlay mode. Thumbnail can not be loaded from: " + thumbUrl);
        }

        try
        {
            overlay   = ImageIO.read(overlayIn);
            shade     = ImageIO.read(shadeIn);
        }
        catch (IOException e)
        {
            log.warn("Cannot read images from resource. Reverting to default mode.");
            return null;
        }

        BufferedImage placeholder = null;
        int thumbWidth, thumbHeight,
            overlayWidth, overlayHeight,
            overlayPadX, overlayPadY;
        if(genericOverlay)
        {
            thumbWidth  = width;
            thumbHeight = (int) (thumbWidth * (overlay.getHeight()/(double)overlay.getWidth()));

            overlayWidth  = (int) (overlay.getWidth() * OVERLAY_MAX_WIDTH) * 2;
            overlayHeight = (int) (overlayWidth * (overlay.getHeight()/(double)overlay.getWidth()));
            overlayPadX   = (int) ((thumbWidth / 2.0) - (overlayWidth / 2.0));
            overlayPadY   = (int) ((thumbHeight / 2.0) - (overlayHeight / 2.0));
        }
        else
        {
            thumbWidth  = width;
            thumbHeight = (int) (thumbWidth * (thumbnail.getHeight()/(double)thumbnail.getWidth()));

            overlayWidth  = (int) (overlay.getWidth() * OVERLAY_MAX_WIDTH);
            overlayHeight = (int) (overlayWidth * (overlay.getHeight()/(double)overlay.getWidth()));
            overlayPadX   = (int) (thumbWidth * OVERLAY_PADDING);
            overlayPadY   = (int) (thumbHeight * OVERLAY_PADDING);

            overlayPadX = thumbWidth - overlayWidth - overlayPadX;
            overlayPadY = thumbHeight - overlayHeight - overlayPadY;
        }

        placeholder  = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = getHighQualityGraphicsRenderMode(placeholder);

        if(!genericOverlay)
        {
            g.drawImage(thumbnail, 0, 0, thumbWidth, thumbHeight, null);
            g.drawImage(shade, 0, 0, thumbWidth, thumbHeight, null);
        }

        g.drawImage(overlay, overlayPadX, overlayPadY, overlayWidth, overlayHeight, null);
        g.dispose();

        return placeholder;
    }

    private Graphics2D getHighQualityGraphicsRenderMode(BufferedImage image)
    {
        Graphics2D g = image.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        return g;
    }
}