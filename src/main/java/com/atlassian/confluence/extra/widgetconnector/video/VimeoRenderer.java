package com.atlassian.confluence.extra.widgetconnector.video;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;

public class VimeoRenderer implements WidgetRenderer
{
    private static final String MATCH_URL = "vimeo.com";
    private static final String PATTERN = "/(\\d+)&?";
    private VelocityRenderService velocityRenderService;

    public VimeoRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        Pattern p = Pattern.compile(PATTERN);
        Matcher m = p.matcher(url);

        String videoId = "";

        if (m.find())
        {
            videoId = m.group(1);
        }
        return "//vimeo.com/moogaloop.swf?clip_id=" + videoId + "&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1";
    }

    public String getTemplate()
    {
        return null;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
