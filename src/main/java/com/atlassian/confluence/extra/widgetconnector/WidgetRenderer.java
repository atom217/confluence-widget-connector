package com.atlassian.confluence.extra.widgetconnector;

import java.util.Map;


public interface WidgetRenderer
{
    /**
     * This method should determine whether the renderer matches a certain url.
     *
     * For example if this renderer should support youtube, the method should
     * match a url like http://youtube.com/photos/atlassian and return true.
     *
     * @param url
     * @return true if the given url matches
     */
    boolean matches(String url);

    /**
     * Returns a String of rendererd HTML, commonly the appropiate embed tag for the given url.
     * The {@link AbstractVelocityRenderer} can be extended to handle the rendering of a velocity template.   
     * @param url
     * @param params
     * @return
     */
    String getEmbeddedHtml(String url, Map<String, String> params);
}
