package com.atlassian.confluence.extra.widgetconnector;

import com.atlassian.confluence.macro.ImagePlaceholder;

import java.util.Map;


public interface RenderManager
{
    String getEmbeddedHtml(String url, Map<String, String> params);

    String getBaseUrl(String url);

    ImagePlaceholder getImagePlaceholder(String url, Map<String, String> params);
}
