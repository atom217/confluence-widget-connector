package com.atlassian.confluence.extra.widgetconnector.widgets;

import com.atlassian.confluence.extra.widgetconnector.WidgetConnectorUtil;
import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.regex.Pattern;

public class GoogleMapsRenderer implements WidgetRenderer
{
    private static final Logger log = LoggerFactory.getLogger(GoogleMapsRenderer.class);

    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/iframe.vm";
    private static final String DEFAULT_WIDTH = "425px";
    private static final String DEFAULT_HEIGHT = "350px";
    private VelocityRenderService velocityRenderService;
    private static final String MATCH_URL = "maps.google.com";

    public GoogleMapsRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        String embedUrl = url + "&output=embed";

        return embedUrl;
    }

    public boolean matches(String url)
    {
        return WidgetConnectorUtil.isURLMatch(url, MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        if (!params.containsKey(VelocityRenderService.WIDTH_PARAM))
            params.put(VelocityRenderService.WIDTH_PARAM, DEFAULT_WIDTH);
        if (!params.containsKey(VelocityRenderService.HEIGHT_PARAM))
            params.put(VelocityRenderService.HEIGHT_PARAM, DEFAULT_HEIGHT);

        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
