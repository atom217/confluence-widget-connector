package com.atlassian.confluence.extra.widgetconnector.video;

import com.atlassian.confluence.extra.widgetconnector.DefaultRenderManager;
import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MySpaceVideoRenderer implements WidgetRenderer
{
    private static final Pattern OLD_MATCH_PATTERN = Pattern.compile("https?://vids.myspace\\.com.*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern MATCH_PATTERN = Pattern.compile("https?://(www.)?myspace\\.com.*$", Pattern.CASE_INSENSITIVE);

    private static final Pattern PATTERN = Pattern.compile("https?://(www.)?myspace.com(/.+)?/video/(.+?)(/.+)?/(\\d+)", Pattern.CASE_INSENSITIVE);

    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/iframe.vm";

    private final VelocityRenderService velocityRenderService;

    private final I18NBeanFactory i18NBeanFactory;

    private final LocaleManager localeManager;

    public MySpaceVideoRenderer(VelocityRenderService velocityRenderService, I18NBeanFactory i18NBeanFactory, LocaleManager localeManager)
    {
        this.velocityRenderService = velocityRenderService;
        this.i18NBeanFactory = i18NBeanFactory;
        this.localeManager = localeManager;
    }

    public String getEmbedUrl(String url)
    {
        Matcher urlMatcher = PATTERN.matcher(url);
        String videoName = "";
        String videoId = "";

        if (urlMatcher.matches())
        {
            videoName = urlMatcher.group(4);
            if (videoName == null || videoName.length() == 0)
                videoName = "/" + urlMatcher.group(3);
            videoId = urlMatcher.group(5);

            if (videoName.length() > 0 && !videoName.endsWith("-"))
                videoName = videoName + "-";
        }

        return "//myspace.com/play/video" + videoName + videoId;
    }

    public boolean matches(String url)
    {
        return MATCH_PATTERN.matcher(url).matches() || OLD_MATCH_PATTERN.matcher(url).matches();
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        if (OLD_MATCH_PATTERN.matcher(url).matches())
        {
            /**
             *  Old MySpace video link should display an error message
             *  Example old link:
             *  * http://vids.myspace.com/index.cfm?fuseaction=vids.individual&amp;videoid=3376248&amp;searchid=20c789f6-1ae9-459a-bfec-75efcfc2847c
             */

            params.put("baseUrlHtml", getText("com.atlassian.confluence.extra.widgetconnector.myspace.outdated.url"));
            params.put(VelocityRenderService.TEMPLATE_PARAM, DefaultRenderManager.ERROR_TEMPLATE);
            return velocityRenderService.render(url, params);
        }

        String protocol = StringUtils.startsWith(url, "https://") ? "https" : "http";

        if (StringUtils.startsWith(url, "/"))
            url = protocol + "://myspace.com" + url;

        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(getEmbedUrl(url), params);
    }

    public String getText(String i18nKey)
    {
        return getI18NBean().getText(i18nKey);
    }

    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.get()));
    }
}
