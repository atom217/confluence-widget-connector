package com.atlassian.confluence.extra.widgetconnector.video;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;


public class YahooVideoRenderer implements WidgetRenderer
{
    private static final String MATCH_URL = "video.yahoo";
    private static final String FLASHVARS_PARAM = "flashVars";
    private static final Pattern PATTERN = Pattern.compile("video\\.yahoo\\.com/watch/([\\d]+)/([\\d]+)");
    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/embed.vm";
    private VelocityRenderService velocityRenderService;
    private static final String EMBED_URL = "//d.yimg.com/static.video.yahoo.com/yep/YV_YEP.swf?ver=2.2.34";

    public YahooVideoRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        params.put(FLASHVARS_PARAM, getFlashVars(url));
        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(EMBED_URL, params);
    }

    public String getFlashVars(String url)
    {
        Matcher m = PATTERN.matcher(url);

        String id = "";
        String vid = "";

        if (m.find())
        {
            vid = m.group(1);
            id = m.group(2);
        }

        return "id=" + id + "&vid=" + vid + "&lang=en-us&intl=us&embed=1";
    }
}
