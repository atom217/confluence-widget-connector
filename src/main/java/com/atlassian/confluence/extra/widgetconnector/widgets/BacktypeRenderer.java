package com.atlassian.confluence.extra.widgetconnector.widgets;

import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BacktypeRenderer implements WidgetRenderer
{
    public static final String SHARED_MODE = "/shared";
    public static final String COMMENTS_MODE = "comments/";
    public static final String MATCH_URL = "backtype.com";

    private static final String VELOCITY_TEMPLATE = "com/atlassian/confluence/extra/widgetconnector/templates/simplejscript.vm";

    public static final Pattern SEARCH_PATTERN = Pattern.compile("comments\\?q=([^/]+)");
    public static final Pattern USER_PATTERN = Pattern.compile("backtype\\.com/([^/]+)");
    public static final Pattern SHARED_PATTERN = Pattern.compile(SHARED_MODE);

    public static final String USER_PLACEHOLDER = "[USER_NAME]";
    public static final String SEARCH_PLACEHOLDER = "[SEARCH_TERM]";

    public static final String USER_URL = "//widgets.backtype.com/" + USER_PLACEHOLDER;
    public static final String SEARCH_URL = "//widgets.backtype.com/comments?q=" + SEARCH_PLACEHOLDER;



    private VelocityRenderService velocityRenderService;


    public BacktypeRenderer(VelocityRenderService velocityRenderService)
    {
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        if (url == null)
            return null;

        if (url.contains(COMMENTS_MODE))
            return renderSearch(url);

        else if (url.contains(SHARED_MODE))
            return renderShared(url);

        else
            return renderUser(url);
    }

    private String renderUser(String url)
    {
        Matcher m = USER_PATTERN.matcher(url);
        if (m.find())
            return USER_URL.replace(USER_PLACEHOLDER, m.group(1));
        return null;
    }

    private String renderShared(String url)
    {
        return renderUser(url) + SHARED_MODE;
    }

    private String renderSearch(String url)
    {
        Matcher m = SEARCH_PATTERN.matcher(url);
        if (m.find())
            return SEARCH_URL.replace(SEARCH_PLACEHOLDER, m.group(1));
        return null;
    }

    public boolean matches(String url)
    {
        return url.contains(MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        params.put(VelocityRenderService.TEMPLATE_PARAM, VELOCITY_TEMPLATE);
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
