package com.atlassian.confluence.extra.widgetconnector.video;

import com.atlassian.confluence.extra.widgetconnector.WidgetConnectorUtil;
import com.atlassian.confluence.extra.widgetconnector.services.HttpRetrievalEmbedService;
import com.atlassian.confluence.extra.widgetconnector.services.VelocityRenderService;
import com.atlassian.confluence.extra.widgetconnector.WidgetRenderer;

import java.util.regex.Pattern;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class DailyMotionRenderer implements WidgetRenderer
{
    private HttpRetrievalEmbedService httpRetrievalEmbedService;

    public static final String MATCH_URL = "dailymotion.com";
    public static final Pattern PATTERN = Pattern.compile("dailymotion&id=/swf/([^\"^&]+)");
    private VelocityRenderService velocityRenderService;


    public DailyMotionRenderer(HttpRetrievalEmbedService httpRetrievalEmbedService, VelocityRenderService velocityRenderService)
    {
        this.httpRetrievalEmbedService = httpRetrievalEmbedService;
        this.velocityRenderService = velocityRenderService;
    }

    public String getEmbedUrl(String url)
    {
        /**
         * CONF-19838 - Dailymotion changed url format for embedded videos
         */
        return url.replaceFirst("/video/", "/swf/video/");
    }

    public boolean matches(String url)
    {
        return WidgetConnectorUtil.isURLMatch(url, MATCH_URL);
    }

    public String getEmbeddedHtml(String url, Map<String, String> params)
    {
        return velocityRenderService.render(getEmbedUrl(url), params);
    }
}
